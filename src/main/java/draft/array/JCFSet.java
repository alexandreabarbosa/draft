package draft.array;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class JCFSet {

	private HashSet<String> coresHashSet = new HashSet<String>();
	private HashSet<String> coresHashSetContains = new HashSet<String>();
	
	private TreeSet<String> coresTreeSet;
	
	public JCFSet() {
		coresHashSet.add("Azul");
		coresHashSet.add("Vermelho");
		coresHashSet.add("Amarelo");
		coresHashSet.add("Verde");
		coresHashSet.add("Preto");
		coresHashSet.add("Branco");
		coresHashSet.add("Marrom");
		
		coresHashSetContains.add("Preto");
		coresHashSetContains.add("Amarelo");
		coresHashSetContains.add("Branco");
		
		coresTreeSet = new TreeSet<String>(coresHashSet);
	}

	public static void main(String[] args) {
		JCFSet jcf = new JCFSet();
		jcf.showHashSet();
		jcf.coresTreeSet();
		System.out.println("\n---x---x---");
	}

	private void coresTreeSet() {
		System.out.println("TreeSet ------------> " + coresTreeSet);
		coresTreeSetDescending();
		coresTreeSetFeatures();
	}

	private void coresTreeSetFeatures() {
		System.out.println("TreeSet First -----> " + coresTreeSet.first() 
						 + "\nTreeSet Last ------> " + coresTreeSet.last()
						 + "\nTreeSet Size ------> " + coresTreeSet.size()
						 + "\nTreeSet pollFirst -> " + coresTreeSet.pollFirst()
						 + "\nTreeSet pollLast --> " + coresTreeSet.pollLast()
						 );
		
	}
	
	private void coresTreeSetDescending() {
		System.out.println();
		System.out.print("TreeSet Descending -> ");
		Iterator<String> iterator = coresTreeSet.descendingIterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + ", ");
		}
		System.out.println("\n");
	}
	
	private void showHashSet() {
		System.out.print("HashSet ----------> ");
		System.out.println(coresHashSet);
		showHashSetAdd();
		showHashSetIterator();
		showHashSetContains();
		showHashSetRetainAll();
		showHashSetClone();
		showHashSetConvertToArray();
		showHashSetConvertedToTreeSet();
	}
	
	public void showHashSetContains() {
		for (String cor : coresHashSet) {
			System.out.println( coresHashSetContains.contains(cor) ? "Contém ---> " + cor : "Não contém " + cor);
		}
	}

	public void showHashSetConvertedToTreeSet() {
		TreeSet<String> treeSet = new TreeSet<String>(coresHashSet);
		System.out.println("HashSet converted to TreeSet => " + treeSet);
	}
	
	public void showHashSetRetainAll() {
		HashSet<String> hashSet = new HashSet<String>();
		hashSet.add("Branco");
		hashSet.add("Marrom");
		hashSet.add("Cinza");
		hashSet.add("Azul");
		hashSet.add("Vermelho");
		hashSet.add("Amarelo");
		hashSet.add("Verde");
		hashSet.add("Preto");
		coresHashSet.retainAll(hashSet);
		System.out.println("HashSet 2 Merge -----> " + hashSet);
		System.out.println("HashSet Merge -----> " + coresHashSet);
	}
	
	public void showHashSetAdd() {
		System.out.println("HashSet ----------> " + coresHashSet);
		coresHashSet.add("Laranja");
		coresHashSet.add("Branco");
		coresHashSet.add("Marrom");
		coresHashSet.add("Cinza");
		coresHashSet.add("Roxo");
		System.out.println("Adding to coresHashSet => " + coresHashSet);
	}

	public void showHashSetIterator() {
		System.out.print("HashSet ----------> ");
		Iterator<String> iterator = coresHashSet.iterator();
		while(iterator.hasNext()) {
			System.out.print(iterator.next() + ", ");
		}
		System.out.println("\n");
	}

	public void showHashSetClone() {
		HashSet<String> hashSet = (HashSet<String>) coresHashSet.clone();
		System.out.println("HashSet Clone --------> " + hashSet);
	}

	public void showHashSetConvertToArray() {
		ArrayList<String> aListConvert = new ArrayList<>(coresHashSet);
		System.out.println("aListConvert ===> " + aListConvert);
	}
}
