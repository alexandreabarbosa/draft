package draft.array;

import java.util.PriorityQueue;

public class JCFQueue {

	private PriorityQueue<String> priorityQueue = new PriorityQueue<String>();
	
	public static void main(String[] args) {
		JCFQueue jcf = new JCFQueue();
		jcf.priorityQueueAdd();
	}
	
	private void priorityQueueAdd() {
		priorityQueue.add("Red");
		priorityQueue.add("Orange");
		priorityQueue.add("Brown");
		priorityQueue.add("Black");
		priorityQueue.add("Purple");
		System.out.println("Adding priority queue ------> " + priorityQueue);
		while( ! priorityQueue.isEmpty() ) {
			priorityQueuePoll();
			System.out.println("pool : " + priorityQueue + ", ");
		}
		PriorityQueue<String> pq2 = new PriorityQueue<String>(priorityQueue);
		priorityQueue.addAll(pq2);
		System.out.println("Adding priority queue 2 ---->" + priorityQueue);
	}

	private void priorityQueuePoll() {
		System.out.println("Queue pooling -------> " + priorityQueue.poll()
		);
	}
}
