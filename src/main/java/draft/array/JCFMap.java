package draft.array;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class JCFMap {

	private HashMap<Integer, String> hashMapStars = new HashMap<Integer, String>();
	private String[] starsName = {"Orion", "Perseus", "Pegasus", "Dorado", "Mintaka", "Aries", "Crux"};
	public static void main(String[] args) {
		JCFMap jcf = new JCFMap();
		jcf.hashMapAdding();
	}
	
	private void hashMapAdding() {
	    Map<String, Integer> budget = new HashMap<>();
	    budget.put("clothes", 120);
	    budget.put("grocery", 150);
	    budget.put("transportation", 100);
	    budget.put("utility", 130);
	    budget.put("rent", 1150);
	    budget.put("miscellneous", 90);
	    System.out.println("map before sorting: " + budget);
	    
	    // let's sort this map by values first
	    Map<String, Integer> sorted = budget
	        .entrySet()
	        .stream()
	        .sorted(Map.Entry.comparingByValue())
	        .collect(
	            Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
	                LinkedHashMap::new));
	 
	    System.out.println("map after sorting by values: " + sorted);
	    
		for (int i = 0; i < starsName.length; i++) {
			hashMapStars.put(i, starsName[i]);
		}
		System.out.println("Adding HashMap ----> " + hashMapStars
				         + "\nHashMap size -----> " + hashMapStars.size()
				);
		System.out.println(hashMapStars);
//		hashMapCopy();
//		hashMapAddAll();
//		hashMapExists();
		
	}

	private void hashMapCopy() {
		HashMap<Integer, String> hashMapCopy = (HashMap<Integer, String>)hashMapStars.clone();
		System.out.println("hashMapCopy -------> " + hashMapCopy);
	}
	
	private void hashMapAddAll() {
		System.out.println("hashMapStars --------> " + hashMapStars);
		HashMap<Integer, String> hashMapAdding = new HashMap<Integer, String>();
		hashMapAdding.put(1, "Orion");
		hashMapAdding.put(2, "Perseus");
		hashMapAdding.put(3, "Pegasus");
		System.out.println("hashMapAdding Antes ----> " + hashMapAdding);
		hashMapAdding.putAll(hashMapStars);
		System.out.println("hashMapAdding Added ----> " + hashMapAdding);
		hashMapAdding.clear();
		System.out.println("hashMapAdding cleaned --> " + hashMapAdding
				+ "\nhashMapAdding is empty? = " + hashMapAdding.isEmpty()
				);
	}
	
	private void hashMapExists() {
		System.out.println("hashMap contain key 2 ? " + hashMapStars.containsKey(2));
		System.out.println("hashMap contain Dorado ? " + hashMapStars.containsValue("Dorado"));
	}
}
