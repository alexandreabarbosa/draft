package draft.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class JCFList {

	private ArrayList<String> cores = new ArrayList<String>();
	private LinkedList<String> coresLinkadas = new LinkedList<String>();
		
	public JCFList() {
		cores.add("Azul");
		cores.add("Vermelho");
		cores.add("Preto");
		cores.add("Amarelo");
		cores.add("Azul");
		cores.add("Verde");
		cores.add("Preto");
		
		coresLinkadas.add("White");
		coresLinkadas.add("Blue");
		coresLinkadas.add("Yellow");
		coresLinkadas.add("Black");
		coresLinkadas.add("Purple");		
		coresLinkadas.add("Brow");		
		coresLinkadas.add("Gray");
		coresLinkadas.add("Orange");		
	}

	public static void main(String[] args) {
		JCFList jcf = new JCFList();
		jcf.showList();
		jcf.showLinkedList();
		
		System.out.println("\n---x---x---");
	}
	
	private void showList() {
		System.out.print("Print --------> ");
		System.out.println(cores);		
		showListWithRemove();
		showListWithIterate();
		showListAdding();
		showListSpecificPosition();
		showListReplaceInTheMiddle();
		showListSearchElement();
		showListSort();
		showListRotate();
		showListShuffle();
		showListShuffle();
	}
	
	private void showListWithRemove() {
		System.out.print("WithRemove ---> ");
		cores.remove(3);
		System.out.println(cores);
	}
	
	private void showListWithIterate() {
		System.out.print("Interate -----> ");
		for (String cor : cores) {
			System.out.println(cor);
		}
	}

	private void showListAdding() {
		System.out.print("Adding  ------> ");
		cores.add("Branco");
		cores.add(0, "Marrom");
		cores.add(cores.size(), "Roxo");
		System.out.println(cores);
	}

	private void showListSpecificPosition() {
		System.out.print("Middle Position " + (cores.size()) + " --> " );
		System.out.println(cores.get(cores.size() / 2));
	}

	private void showListReplaceInTheMiddle() {
		System.out.print("Updating ------> ");
		cores.set(2, "Laranja");
		System.out.println(cores);
	}

	private void showListSearchElement() {
		System.out.print("Searching Roxo > ");
		System.out.println(cores.contains("Roxo"));
	}

	private void showListSort() {
		System.out.print("Sorting -------> ");
		Collections.sort(cores);
		System.out.println(cores);
		Comparator<String> comparator = Collections.reverseOrder();
		Collections.sort(cores, comparator);
		System.out.print("Sorting Des ---> ");
		System.out.println(cores);
	}
	
	private void showListShuffle() {
		System.out.print("Shuffle -------> ");
		Collections.shuffle(cores);
		System.out.println(cores);
	}

	private void showListRotate() {
		System.out.print("Rotate -------> ");
		Collections.rotate(cores, 1);
		System.out.println(cores);
	}
	
	private void showLinkedList() {
		System.out.print("Linked List -------> ");
		System.out.println(coresLinkadas);
		showLinkedListDescendingInterator();
		showLinkedListSwap();
	}
	
	private void showLinkedListDescendingInterator() {
		System.out.print("Linked List Descending ---> ");
		Iterator<String> iterator = coresLinkadas.descendingIterator();
		while (iterator.hasNext()) {
			System.out.print(iterator.next() + ',');
		}
		System.out.println("\n");		
	}
	
	private void showLinkedListSwap() {
		System.out.print("Linked List Swap ------> ");
		Collections.swap(coresLinkadas, 0, coresLinkadas.size()-1);
		System.out.println(coresLinkadas);
		List<String> aList = new ArrayList<String>(coresLinkadas);
		List<String> lList = new LinkedList<String>(aList);
	}
}
