import java.util.ArrayList;

public class Palindrome {

	public static void main(String[] args) {
		Palindrome palindrome = new Palindrome();
		System.out.println(palindrome.isAmostPalindrome("natan"));
		System.out.println(palindrome.isAmostPalindrome("word"));
	}

	private boolean isAmostPalindrome(String str) {
		boolean result;
		StringBuffer aList = new StringBuffer();
		for (int i = 0; i < str.length(); i++) {
			aList.append(str.substring(i, i+1));
		}

		StringBuffer aListReverse = new StringBuffer();
		for (int i =  str.length(); i > 0; i--) {
			aListReverse.append(str.substring(i-1, i));
		}

		System.out.println(aListReverse);
		System.out.println(aList);
		result = aListReverse.toString().equals(aList.toString());
		
		return result;
	}
}
