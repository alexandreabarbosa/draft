import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

public class PAMissingNumber {
	//private List<Integer> listInteger = Arrays.asList(1, 2, 3, 4, 6, 7, 8, 9, 10);	//imagine that this list is huge
	private List<Integer> listInteger = Arrays.asList(3, 4, 5, 6, 8, 9, 10);	//imagine that this list is huge
	public static void main(String[] args) {
		//new PAMissingNumber().exec();
		//new PAMissingNumber().eugeneSolution();
		new PAMissingNumber().bitSet();
	}

	void exec() {
		Collections.sort(listInteger);
		int result = 0;
		int sequence = listInteger.get(0);
		for (int i = 0;	i < listInteger.size(); i++) {
			if (sequence!= listInteger.get(i)) {
				result = sequence;
				break;
			} else {  
				sequence++;
			}
		}
		System.out.println("The missing number is : " + result);
	}
	void eugeneSolution() {
		 int max = Collections.max(listInteger);
		 int sum = max * (max + 1) / 2;
		 int diff = sum - listInteger.stream().mapToInt(Integer::intValue).sum();
		 System.out.println(diff);		
	}
	
	void bitSet() {
		BitSet bs = listInteger.stream().collect(BitSet::new, BitSet::set, BitSet::or);
		System.out.println(bs.nextClearBit(bs.nextSetBit(0)));
	}
}
