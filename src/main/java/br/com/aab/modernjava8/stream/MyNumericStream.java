package br.com.aab.modernjava8.stream;

import java.util.Arrays;
import java.util.List;

public class MyNumericStream {
   List<Integer> listInts = Arrays.asList( 25, 30, 35, 40, 45);
   
   public static void main(String[] args) {
      new MyNumericStream().sumInts();
   }
   private void sumInts() {
      int intStreamVal = listInts.stream().reduce(0, Integer::sum);
      System.out.println(intStreamVal);
   }
}
