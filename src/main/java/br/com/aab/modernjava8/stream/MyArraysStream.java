package br.com.aab.modernjava8.stream;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class MyArraysStream {

	private List<Integer> listIntegerNumbers = Arrays.asList(32, 128, 64, 512, 4096, 1024, 16, 2048);

	public static void main(String[] args) {
		new MyArraysStream().summingElements();
	}

	void exec1() {
		String[] words = { "Parallel", "Successfully" };
		Stream<String> streamStr = Arrays.stream(words);

	}

	void summingElements() {
		listIntegerNumbers.forEach(System.out::println);
		Integer result = listIntegerNumbers.parallelStream().reduce(0, Integer::sum);
		System.out.println(result);
		Optional<Integer> resultOptional = listIntegerNumbers.parallelStream().reduce((a, b) -> (a + b));
		System.out.println("Sum:" + resultOptional);

		Optional<Integer> resultMax = listIntegerNumbers.parallelStream().reduce(Integer::max);
		System.out.println("Max:" + resultMax);

		Optional<Integer> resultMin = listIntegerNumbers.parallelStream().map(d -> 1).reduce((a, b) -> (a + b));
		System.out.println("Min:" + resultMin);

		int resultCount = listIntegerNumbers.parallelStream().reduce(0, (a, b) -> (a + b));
		System.out.println("Count:" + resultCount);

		long longCountStream = listIntegerNumbers.parallelStream().count();
		System.out.println("Count Paralle Stream:" + longCountStream);

	}

}
