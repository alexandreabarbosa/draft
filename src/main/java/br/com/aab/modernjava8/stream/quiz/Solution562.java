package br.com.aab.modernjava8.stream.quiz;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import br.com.aab.modernjava8.stream.model.Trader;
import br.com.aab.modernjava8.stream.model.Transaction;

public class Solution562 {

	public static void main(String[] args) {
		new Solution562().init();
	}

	private void init() {
		Trader raoul = new Trader("Raoul", "Cambridge");
		Trader mario = new Trader("Mario","Milan");
		Trader alan = new Trader("Alan","Cambridge");
		Trader brian = new Trader("Brian","Cambridge");
		List<Transaction> transactions = Arrays.asList(
		    new Transaction(brian, 2011, 300),
		    new Transaction(brian, 2011, 900),
		    new Transaction(raoul, 2012, 1000),
		    new Transaction(raoul, 2011, 400),
		    new Transaction(mario, 2012, 710),
		    new Transaction(mario, 2012, 700),
		    new Transaction(alan, 2012, 950)
		);
		
		List<Transaction> tr2011 = transactions
				.parallelStream()
				.filter(t -> t.getYear() == 2011)
				.sorted(Comparator.comparing(Transaction::getValue))
				.collect(Collectors.toList());
		tr2011.forEach(t -> {
			System.out.println("5.1 Finds all transactions in 2011 and sort by value (small to high) = " 
					+ t.getYear() + " / " + t.getValue() + " / " 
					+ t.getTrader().getName() + " / " + t.getTrader().getCity());
		});
		
		Set<String> cities = transactions
				.parallelStream()
				.map(t -> t.getTrader().getCity())
				.distinct()
				.collect(Collectors.toSet());
		System.out.println("\n5.2 What are all unique cities where the traders work using java.util.Set interface?");
		cities.forEach(System.out::println);
				
		List<Trader> fromCambridge = transactions
				.stream()
				.map(Transaction::getTrader)
				.filter(t -> ("Cambridge").equals(t.getCity()))
				.distinct()
				.sorted(Comparator.comparing(Trader::getName))
				.collect(Collectors.toList());
		System.out.println("\n5.3 Finding all traders from Cambridge and sort them by name");
		fromCambridge.forEach(t -> {
			System.out.println(t.getCity() + " / " + t.getName());
		});
		
		String strTradersName = transactions
				.parallelStream()
				.map(t -> t.getTrader().getName())
				.distinct()
				.sorted()
				//.reduce(" ", (n1, n2) -> n1 + n2)
				.collect(Collectors.joining())
				;
		System.out.println("\n5.4 Returning a string with all Traders names sorted alphabetically"
				+ "\n"
				+ strTradersName);
		
		boolean tradersInMilan = transactions
				.stream()
				.anyMatch(t -> "Milan".equals(t.getTrader().getCity()))
				;
		System.out.println("5.5 Are there any Traders in Milan? " + tradersInMilan);

		System.out.println("5.6 - Printing all transactions's values from the traders living in Cambridge");
		transactions.stream() 
				.filter(t -> "Cambridge".equals(t.getTrader().getCity()))
				.map(Transaction::getValue)
				.forEach(System.out::println);
		
		Optional<Integer> maxValue = transactions
				.parallelStream()
				.map(Transaction::getValue)
				.reduce(Integer::max);
		System.out.println("\n5.7 What's the highest value of all transactions = " + maxValue.get());
		
		Optional<Integer> smallestValue = transactions
				.parallelStream()
				.map(Transaction::getValue)
				.reduce(Integer::min);
		System.out.println("\n5.8 What's the smallest value of all transactions = " + smallestValue.get());
	}

	
	
}
