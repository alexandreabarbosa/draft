package br.com.aab.camelinaction2.stream.mapping;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Mapping {

	public static void main(String[] args) {
		new Mapping().wordsCounter();
	}

	private void wordsCounter() {
		List<String> words = Arrays.asList("Modern", "Java", "In", "Action");
		List<Integer> wordLengths = words
				.stream()
				.map(String::length)
				.collect(Collectors.toList());
		//wordLengths.parallelStream().forEach(System.out::println);
		
		List<String> uniqueChars = words.stream()
				.map(w -> w.split(""))
				.flatMap(Arrays::stream)
				.distinct()
				.collect(Collectors.toList());
		uniqueChars.stream().forEach(System.out::println);
	}
}
