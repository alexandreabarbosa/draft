package br.com.aab.camelinaction2.stream;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
public class Dish {
	private final String name;
	private final Boolean vegetarian;
	private final int calories;
	private final Type type;
	
	public enum Type {MEAT, FISH, OTHER}
}
