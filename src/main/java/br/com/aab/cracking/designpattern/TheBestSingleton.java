package br.com.aab.cracking.designpattern;

public class TheBestSingleton {
	
	public static final TheBestSingleton instance = new TheBestSingleton();
	
	private TheBestSingleton() {}
	
	public static class TheBestSingletonHelper {
		private static final TheBestSingleton INSTANCE = new TheBestSingleton();

		public TheBestSingleton getInstance() {
			return INSTANCE;
		}
	}

}
