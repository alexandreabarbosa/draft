package br.com.aab.cracking.whizlabs.practisetest2;

class A {
	private void run() {
		System.out.println("A");
	}
}

public class Whiz40 extends A{

	private void run() {
		System.out.println("Overriding A");
	}
	public static void main(String[] args) {
		new Whiz40().run();
	}

}
