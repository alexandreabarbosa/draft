package br.com.aab.cracking.whizlabs.practisetest1;

class Sup {
	String s = "";
	
	Sup() {
		s += "sup ";
	}
}

class Sub extends Sup {
	Sub() {
		s += "sub ";
	}
}

public class Whiz63 extends Sup {

	public Whiz63() {
		s += "subsub";
	}
	
	public static void main(String[] args) {
		System.out.println(new Whiz63().s);
	}

}
