package br.com.aab.cracking.whizlabs.practisetest3;

public class Whiz61 {

	static {
		x = 10;
		y = 5;
	}
	
	static int x; //erro int x; 
	final static int y;
	
	public static void main(String[] args) {
		try {
			Whiz61 pr = new Whiz61();
			int c = pr.x/y;
			System.out.println(c);
		} catch (ArithmeticException ae) {
			System.out.println("Arithmetic Excepiton");
		}
	}

}
