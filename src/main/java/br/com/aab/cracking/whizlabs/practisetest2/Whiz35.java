package br.com.aab.cracking.whizlabs.practisetest2;

public class Whiz35 {

	public static void main(String[] args) {

		float f2 = 1.000000001f;
		float f1 = 0.000000001f;
		float f3 = 1.000000000f;
		
		System.out.println((f2 - f1) == f3);
		
	}

}
