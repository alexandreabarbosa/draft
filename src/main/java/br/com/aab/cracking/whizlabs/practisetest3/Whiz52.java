package br.com.aab.cracking.whizlabs.practisetest3;

public class Whiz52 {

	public static void main(String[] args) {
		double d = new Divider().divide(30,3);
		System.out.println(d);
	}

}

class Divider {
	double divide(int i, int j) {
		return i/j;
	}
}