package br.com.aab.cracking.whizlabs.practisetest4;

import java.util.Arrays;

public class Whiz4 {

	public static void main(String[] args) {
		int[] x = new int[5];
		Arrays.fill(x, 2018);
		for (int i : x) {
			System.out.println(i);
		}
	}

}
