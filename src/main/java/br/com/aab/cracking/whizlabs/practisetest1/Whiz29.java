package br.com.aab.cracking.whizlabs.practisetest1;

public class Whiz29 {

	public static void main(String[] args) {
		float f2 = 1.000001f;
		float f1 = 0.000001f;
		float f3 = 1.000000f;
		
		System.out.println((f2 - f1) == f3);
		
	}

}
