package br.com.aab.cracking.whizlabs.diagnostic;

public class Whiz66 {

	public static void main(String[] args) {
		char[] val1 = { '1', 'Z', '0', '-', '8', '1' };
		StringBuilder sb = new StringBuilder();
		sb.append(val1, 0, val1.length -1);
		System.out.println(sb.toString());
		sb.append("08");
		sb.setLength(5);
		sb.insert(5,  "10");
		System.out.println(sb.toString());
		String strB = new String(sb);
		System.out.println(strB);
		StringBuffer sbuffer = new StringBuffer(strB);
		String strF = new String(sbuffer);
		System.out.println(strF);
	}

	static void exec1(Object o) {
		String[] retorno = (String[]) o;
		for (int x = 5, y = 0; x > 0; x--, y++) {
			retorno[y] = Integer.toString(x);
		}
	}
}
