package br.com.aab.cracking.whizlabs.practisetest2;

abstract class Animal {
	void run() {
		System.out.println("Animal run()");
	}
	abstract void sound();
}

class Dog extends Animal {
	void sound() {
		System.out.println("Bark");
	}
	
	public void run() {
		System.out.println("Dog runs");
	}
}

public class Whiz41 {

	public static void main(String[] args) {
		Animal dog = new Dog();
		dog.sound();
		dog.run();
		
		Long value = new Long(5);
		
	}

}
