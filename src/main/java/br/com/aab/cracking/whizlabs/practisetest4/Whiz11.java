package br.com.aab.cracking.whizlabs.practisetest4;

public class Whiz11 {

	public static void main(String[] args) {
		try {
			System.out.println(args[0]);
		} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException e) {
			if (e instanceof ArrayIndexOutOfBoundsException) {
				//erro e = new ArrayIndexOutOfBoundsException("Out of bounds");
			} else if (e instanceof NullPointerException) {
				//erro e = new NullPointerException("Null value");
			} else {
				//erro e = new ArithmeticException("Arithmetic");
			}
		}
	}

}
