package br.com.aab.cracking.whizlabs.practisetest4;

public class Whiz15 {

	public static void main(String[] args) {
		try {
			int a[] = new int[4];
			a[3] = (a[1] + a[1]) / a[2];
			System.out.println(a[3]);
		} catch (ArithmeticException ae) {
			System.out.println("A");
		} catch (Exception e) {
			System.out.println("E");
		}
	}

}
