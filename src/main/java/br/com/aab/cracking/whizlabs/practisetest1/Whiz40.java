package br.com.aab.cracking.whizlabs.practisetest1;

interface I { void method(); }

class A implements I {
	void A(String s) {}
	
	public void method() { System.out.println("A"); }
}

class C extends A implements I {
	public void method() { System.out.println("C"); }
}

public class Whiz40 {

	public static void main(String[] args) {
		A a = new A();
		C c1 = (C)a;    
		a.method();
	}

}
