package br.com.aab.cracking.whizlabs.practisetest1;

public class Whiz35 {

	public static void main(String[] args) {
		int arr[][] = {{1, 3, 5}, {7, 8}};
		out: for (int []a : arr) {
			for (int i : a) {
				if (i == 7) continue;
				System.out.println(i);
				if (i == 3) break;
			}
		}
	}

}
