package br.com.aab.cracking.whizlabs.practisetest2;

public class Whiz29 {

	public static void main(String[] args) {
		
		float f1 = new Float(3.15);
		Float f2 = new Float(3.15);
		Float f3 = 3.15f;
		float f4 = 3.15F;
		
		double d1 = new Double(10);
		double d2 = 10;
		double d3 = new Double(10.1);
		double d4 = 10.1;
		
		final int array[] = {1, 2, 3};
		switch(1) {
			case 1: System.out.println("A");
			case 2: System.out.println("B");
			default: {System.out.println("default");}
			case 3: System.out.println("C");
		}
	}

}
