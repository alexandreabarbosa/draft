package br.com.aab.cracking.whizlabs.practisetest1;

public class Whiz47 {

	private static int classVariable;
	private int instanceVariable;
	
	public static void main(String[] args) {
		Whiz47 w = new Whiz47();
	}

	void execute() {
		Float float1 = 9f;
		Double double1 = 9D;
		
		float float2 = 8f;
		double double2 = 8f;
		
		int localVariable;
		System.out.println(classVariable);		
		System.out.println(instanceVariable);
//erro		System.out.println(localVariable);
		//Erro... 
//		System.out.print();
		//Ok ...
		System.out.println();
	}
}

