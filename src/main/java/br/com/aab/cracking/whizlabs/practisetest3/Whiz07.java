package br.com.aab.cracking.whizlabs.practisetest3;

import java.util.Arrays;

public class Whiz07 {

	public static void main(String[] args) {
		int[] ints = {3,6,1,4,0, 9, 7, 12};
		Arrays.sort(ints, 0, 4);
		for (int i : ints) {
			System.out.print(i);
		}
	}

}
