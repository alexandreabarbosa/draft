package br.com.aab.cracking.whizlabs.practisetest1;

public class Whiz02 {

	public static void main(String[] args) {
		//long[][] l2d = {{1,3,4},{7,8,3},{9, 7, 1, 4}};
		long[][] l2d;
		long[] l1d = {1, 2, 3};
		Object o = l1d;
		l2d = new long[3][3];
		l2d[0] = (long[])o;
		for (long l : l1d) {
			System.out.print(l);			
		}
		System.out.println("");
		for (long[] l : l2d) {
			for (long m : l) {
				System.out.print(m);							
			}
		}
	}

}
