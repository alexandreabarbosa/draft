package br.com.aab.cracking.whizlabs.practisetest3;

public class Whiz33 {

	public static void main(String[] args) {
		for (int x = 10; x > 5; x++) {
			if (x == 16) x -= 11;
			System.out.print(x + " ");
		}
	}

}
