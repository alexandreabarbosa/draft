package br.com.aab.cracking.whizlabs.practisetest2;

public class Whiz52 {

	static int x = 0b1;
	static int y = 0xF;
	static int z = 017;
	
	public static void main(String[] args) {
		System.out.println(x + " + " + y + " + " + z + " = " + (x + y + z));
		
	}

}
