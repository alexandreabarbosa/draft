package br.com.aab.cracking.whizlabs.practisetest2;

public class Whiz56 {

	public static void main(String[] args) {
		Integer wrapperInteger = 10;
		Double wrapperDouble = 10d;
		System.out.println(wrapperInteger.equals(wrapperDouble));
		
		Integer int1 = 10;
		int int2 = 10;
		Double doubleComInt = 20d;
		
		System.out.print("Integers => ");
		System.out.println(int1 == int2);
		
		Double double1 = 20d;
		double double2 = 20;
		
		System.out.print("Doubles usando == ");
		System.out.println(double1 == double2);
		System.out.print("Doubles usando equals ");
		System.out.println(double1.equals(double2));		

		System.out.print("Integer.equals(double) = ");
		System.out.println(int1.equals(doubleComInt));

		Float float1 = 9.948574f;
		float float2 = 9.948574f;
		System.out.print("Floats => ");		
		System.out.println(float1 == float2);
		
		Long long1 = 8398020820l;
		long long2 = 8398020820l;
		System.out.print("Longs => ");		
		System.out.println(long1 == long2);
		
		System.out.println(Integer.MAX_VALUE);
		
		Character caracter = 'C';
		caracter++;
		
		System.out.println(caracter);
	}

}
