package br.com.aab.cracking.whizlabs.practisetest2;

import static java.lang.Math.abs;
import static java.lang.System.out;

public class Whiz11 {

	public static void main(String[] args) {
		try {
			abs(1);
			out.println("ddd");
			new Whiz11().method();
		} catch (ArithmeticException ae) {
			System.out.println("Arithmetic!");
		} catch (Exception e) {
			System.out.println("Exception!");			
		} finally {
			System.out.println("Finally 2");
		} 
	}
	
	public void method() throws ArithmeticException {
		for (int x = 0; x < 5; x++) {
			int y = (int) 5/x;
			System.out.println(y);
		}
	}

}
