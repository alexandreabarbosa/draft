package br.com.aab.cracking.whizlabs.practisetest2;

interface I {
	void method();
}

class A1 implements I {
	void A1(String s) {}
	
	public void method() {
		System.out.println("A");
	}
}

class C extends A1 implements I {
	public void method() {
		System.out.println("C");
	}
}

public class Whiz12 {

	public static void main(String[] args) {
		A1 a = new A1();
		C c1 = (C)a;
		c1.method();
	}

}
