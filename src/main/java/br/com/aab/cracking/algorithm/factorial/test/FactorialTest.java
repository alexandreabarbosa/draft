package br.com.aab.cracking.algorithm.factorial.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.aab.cracking.algorithm.factorial.Factorial;

public class FactorialTest {

	@Test
	public void testFactorial() {
		assertEquals(24, new Factorial().getFactor(4));
		assertEquals(120, new Factorial().getFactor(5));
	}
}
