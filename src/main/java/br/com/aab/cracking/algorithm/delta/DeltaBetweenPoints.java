package br.com.aab.cracking.algorithm.delta;

public class DeltaBetweenPoints {

    public static void main(String[] args) {
        //(x2 - x1)^2 + (y2 - y1)^2 = d^2
    	DeltaBetweenPoints dbp = new DeltaBetweenPoints();
    	System.out.println(dbp.averageDistanceThreePoints(2, 3, 2, 5, 4, 8));
    }

    public float averageDistanceThreePoints(final float x1, final float y1, final float x2, final float y2, final float x3, final float y3) {
    	float result = 0;
    	float dPoint12 = calc(x1, y1, x2, y2);
    	float dPoint23 = calc(x2, y2, x3, y3);
    	result = dPoint12 + dPoint23;
        System.out.println(result);
        return result;
    }
    
    private float calc(final float x1, final float y1, final float x2, final float y2) {
        float deltaBetween = 0, x = 0, y = 0, delta = 0;
        x = x2 - x1;
        y = y2 - y1;
        delta = (float) Math.pow((x + y), 2);
        deltaBetween = (float) Math.sqrt(delta);
        return deltaBetween;
    }
}