package br.com.aab.cracking.algorithm.factorial;

public class Factorial {

	public int getFactor (int factor) {
		int result = 1;
		if (factor == 2) {
			result = 2;
		} else if (factor > 2) {
			result = factor;
			while (factor > 2) {
				result *= --factor;
			}
		}
		return result;
	}
}
