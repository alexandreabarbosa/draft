package br.com.aab.cracking.algorithm.intersection;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayIntersectionInOut {

	private static ArrayList<Integer> interseccao = new ArrayList<Integer>();
	private static ArrayList<Integer> discrepancia = new ArrayList<Integer>();

	public static void main(String[] args) {
		ArrayIntersectionInOut intersection = new ArrayIntersectionInOut();
		Integer[] cj1 = {1, 2, 3, 4, 5, 6, 7};
		Integer[] cj2 = {3, 6};
		intersection.execute(cj1, cj2);
		System.out.println("Intersecção => " + interseccao);
		System.out.println("Discrepancia => " + discrepancia);
	}

	private void execute(Integer[] conjuntoInt1, Integer[] conjuntoInt2) {
		
		ArrayList<Integer> conjuntoAList1 = new ArrayList<>();
		ArrayList<Integer> conjuntoAList2 = new ArrayList<>();
		
		Collections.addAll(conjuntoAList1, conjuntoInt1);
		Collections.addAll(conjuntoAList2, conjuntoInt2);
		System.out.println("Conjunto 1 => " + conjuntoAList1);
		System.out.println("Conjunto 2 => " + conjuntoAList2);
		
		for (int i : conjuntoAList1) {
			if (conjuntoAList2.contains(i)) {
				interseccao.add(i);
			} else {
				discrepancia.add(i);
			}
		}
		
		

	}
}
