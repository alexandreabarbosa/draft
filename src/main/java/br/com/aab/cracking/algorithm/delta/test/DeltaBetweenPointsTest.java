package br.com.aab.cracking.algorithm.delta.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import br.com.aab.cracking.algorithm.delta.DeltaBetweenPoints;

public class DeltaBetweenPointsTest {

	@Test
	public void testDeltas() throws Exception {
		assertEquals(7F, new DeltaBetweenPoints().averageDistanceThreePoints(2, 3, 2, 5, 4, 8));
	}
	
}
