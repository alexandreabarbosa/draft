package br.com.aab.cracking.studyguide;

public class GeneralTests {


	public static void main(String[] args) {
		run12();
	}
	
	static private void run13() {
		int[][] intValues = new int[3][2];
		for (int i = 0; i < intValues.length; i++) {
			for (int j = 0; j < intValues[i].length; j++) {
				intValues[i][j] = i + j;
				System.out.println(" Valor [" + i + "][" + j + "] = " + intValues[i][j]);
			}
		}
	}
	
	static private void run12() {
		int[][] intValues = { {12, 2, 1969}, {22, 07, 1987}, {449, 268, 9796}, {511, 218, 5398} };
		int sampleArrayA[][] = { {1, 2, 3, 6}, {3, 5, 8} };
		int lenghtArrayA = sampleArrayA.length;
		System.out.println("lenghtArrayA = " + lenghtArrayA);
		System.out.println("intValues = "+ intValues.length);
	}
	
	static private void run11() {
		boolean stillRunning = true;
		if ( stillRunning = true ) run10();
	}
	
	static private void run10() {
		boolean value = false;
		System.out.println(false || (value = true));
	}
	
	static private void run9() {
		System.out.println(3 + 2 + "A" + 1 + (1+1) + 1 + 1);
	}
	
	static private void run8() {
		StringBuffer sb = new StringBuffer("magic");
		//sb.append("al").replace(1, 3, "us").matches("musical");
		System.out.println(sb);
		
	}
	
	static private void run7() {
		String string = "Dollar bill";
		string = string.replace("Dollar bill", "Silver dollar");
		if ("Dollar bill".equals(string)) {
			System.out.println("I have a dollar bill");
		} else {
			System.out.println("I have a silver bill");
		}
	}

	static private void run6() {
		String svar = "  AlexAndre Antonio Barbosa  ";
		System.out.println(svar.trim());
	}
	
	static private void run5() {
		int x, y, u, v = 0;
		x = 9;
		y = 14;
		u = 14;
		v = 20;
		String a = "Supercalifragilisticexpialidocious!";
		String b = a.substring(x, y);
		char[] c = {a.charAt(u), a.charAt(v)};
		System.out.println(b + String.valueOf(c));
	}
	
	static private void run4() {
		String java = "Java";
		StringBuffer sb1 = new StringBuffer(java.toString());
		System.out.println(java == sb1.toString());
		System.out.println(java.equals(sb1.toString()));
		System.out.println(sb1.toString().equals(java));
	}
	
	static private void run3() {
		String titulo = "meu nome é ";
		String meunome = titulo.concat("Alexandre");
		System.out.println(meunome);
	}
	
	static private void run2() {
		String svar = "  AlexAndre Antonio Barbosa  ";
		String strim = svar.trim();
		System.out.println("[" + strim + "]");
		System.out.println(strim.trim().substring(4, 6));
		System.out.println(strim.trim().substring(4));
		
		String string1 = "1";
		String string2 = string1.concat(svar);
		svar.concat(string1);
		System.out.println("[" + string2 + "]");
		System.out.println("[" + svar + "]");
		
	}
	
	static private void run1() {
				
		byte bvar = 10;
		int ivar = 20;

		int a = 15;
		int b = 20;
		int c = b = a = 32;		
		System.out.println("c = " + c + " / b = " + b + " / a = " + a);
		
		float fvar1 = 29.853f;
		float fvar2 = bvar * fvar1;
		System.out.println(fvar2);

		float fvar3 = ivar + fvar1;
		System.out.println(fvar3);
				
		double dvar1 = 83.4829;
		double dvar2 = dvar1 * bvar;
		System.out.println(dvar2);
	}
}
