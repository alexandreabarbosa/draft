package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question31of70 {

	public static void main(String[] args) {
		
		final int x = 0;
		final int y = 2;
		
		switch ((x+y)*2) {
		case x: System.out.println("A");
		case 1: System.out.println("B");
		default: System.out.println("default");break;
		case y: System.out.println("C");
		}
	}

}
