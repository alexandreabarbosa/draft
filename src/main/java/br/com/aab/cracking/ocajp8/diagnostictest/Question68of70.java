package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question68of70 {

	public static void main(String[] args) {
		char[] chars = {'1','Z','0','-','8','1'};
		StringBuffer sb = new StringBuffer();
		sb.append(chars, 0, chars.length-1);
		sb.append("08");
		sb.setLength(4);
		//sb.insert(5, "10");
		
		String name = "Alexandre Antonio Barbosa";
		System.out.println(name.lastIndexOf(20));
		
		Double dou = new Double(3);
		Object obj = new Long(9);
		Double double2 = 3D;
	}

}
