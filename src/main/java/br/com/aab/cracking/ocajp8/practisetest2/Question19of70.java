package br.com.aab.cracking.ocajp8.practisetest2;

public class Question19of70 {

	static int x = 2;
	static int z;
	
	public static void main(String[] args) {
		System.out.println(x+z);
	}

	static {
		int x =  3;
		z = x;
		System.out.println(x);
		System.out.println(z);
	}
}
