package br.com.aab.cracking.ocajp8.practisetest5;

public class Question12 {

	public static void main(String[] args) {
		Integer a = new Integer(127);
		Integer b = new Integer(127);
		Integer c = 127;
		Integer d = 127;
		Integer e = new Integer(200);
		Integer f = new Integer(200);
		Integer g = 200;
		Integer h = 200;
		
		System.out.println((a == b) + " " + (c == d) + " " + (e == f) + " " + (g == h));
		System.out.println((a.equals(b)) + " " + (c.equals(d)) + " " + (e.equals(f)) + " " + (g.equals(h)));
	}

}
