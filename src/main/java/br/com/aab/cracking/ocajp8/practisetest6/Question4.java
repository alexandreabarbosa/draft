package br.com.aab.cracking.ocajp8.practisetest6;

import java.util.Arrays;

public class Question4 {

	public static void main(String[] args) {
		int[] ints = {1,2,3,4,5,6,7, -2, -1, -5};
		int[] inteiros = Arrays.copyOf(ints, 10);
		
		System.out.println("Search for 4 - before sort - index = " + Arrays.binarySearch(inteiros, 4));
		for (int i = 0; i < inteiros.length; i++)
			System.out.print(inteiros[i] + " ");
		
		Arrays.sort(inteiros);
		System.out.println("\nSearch for 4 - after sort - index = " + Arrays.binarySearch(inteiros, 4));
		for (int i = 0; i < inteiros.length; i++)
			System.out.print(inteiros[i] + " ");
	}

}
