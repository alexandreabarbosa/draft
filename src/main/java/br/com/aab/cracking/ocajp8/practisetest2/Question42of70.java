package br.com.aab.cracking.ocajp8.practisetest2;

class Animalzinho {
	public void eat() throws Exception { System.out.println("Animal eats"); }
}

public class Question42of70 extends Animalzinho {

	public void eat() { System.out.println("Dog eats"); }
	
	public static void main(String[] args) {
		Animalzinho a = new Question42of70();
		Question42of70 q = new Question42of70();
		q.eat();
//		try {
//			a.eat();
//		} catch (Throwable e) {
//		} finally {
//			
//		}
		/** 
		 * a.eat(); //ERRO por que na pai tem o throws Exception e o filho não tem e 
		 * não posso "reduzir o escopo"
		 * uma alternativa eh implementar o try/catch
		 */
	}

}
