package br.com.aab.cracking.ocajp8.test54;

public class Program {

		public static void main(String[] args) {
			Print p = new Print();
			p.print(6);
			Program prg = new Program();
			prg.teste();
		}
		
		private void teste() {
			Print p = new Print();
			p.print(9);
			int i = 1000_00_0;
			System.out.println(i);
			
			Character char1 = new Character('9');
			System.out.println(char1);
			Double teste = 0.0/0.0;
			System.out.println(teste.isNaN() ? 0 : teste);
		}
}

class Print{
	private static void p2(int i) {
		System.out.println( i * 2 );
	}
	
	static void print(int i) {
		System.out.println(i);
	}
}
