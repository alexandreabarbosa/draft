package br.com.aab.cracking.ocajp8.practisetest4;

public class Question11 {

	public static void main(String[] args) {
		try {
			System.out.println(args[0]);
		} catch (ArrayIndexOutOfBoundsException | ArithmeticException | NullPointerException e) {
			if (e instanceof ArrayIndexOutOfBoundsException) {
//				e = new ArrayIndexOutOfBoundsException("Out of bounds");
			} else if (e instanceof NullPointerException) {
//				e = new NullPointerException("Null Value");
			} else {
//				e = new ArithmeticException("Arithmetic");   ERRO e cannot be assigned
			}
		}
	}

}
