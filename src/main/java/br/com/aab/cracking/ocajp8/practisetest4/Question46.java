package br.com.aab.cracking.ocajp8.practisetest4;

interface Run {
	static int range = 12;
	public static double randomize() {
		return Math.random() * range;
	}
}

public class Question46 {

	public static void main(String[] args) {
		System.out.println(Run.randomize());
	}

}
