package br.com.aab.cracking.ocajp8.practisetest2;

interface Ii {
	public default void print() {
		System.out.println("I");
	}
	
	static void method() {
		System.out.println("Static");
	}
	
}
public class Question49of70 {

	public static void main(String[] args) {
		
		Long long1 = 0x99ffCl;
		System.out.println("Long eh " + long1);
		Ii i = new Ii() {};
		i.print();
		Ii.method();
	}

}
