package br.com.aab.cracking.ocajp8.practisetest2;

public class Question9of70 {

	public static void main(String[] args) {
		int x = 5, y = 10;
		try {
			y /= x;
		} catch (Exception e) {
			System.out.println("error");
		} finally {
			System.out.println("finally");
		}
	}

}
