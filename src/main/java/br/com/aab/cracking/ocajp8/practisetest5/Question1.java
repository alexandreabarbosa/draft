package br.com.aab.cracking.ocajp8.practisetest5;

public class Question1 {

	public static void main(String[] args) {
		new Question1().exec(new char[] {'A', 'B', 'C', 'D', 'E'});
	}
	
	void exec(char[] param) {
		System.out.println(param);
	}
}
