package br.com.aab.cracking.ocajp8.practisetest3;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
class Animal {
	int age = 0;
	int numerFoots = 0;
}

class Cat extends Animal {
}

class Dog extends Animal {	
}

public class Question44 {

	public static void main(String[] args) {
		Cat c = new Cat();
		Animal a = c;
		Animal a2 = new Cat();
		
		System.out.println("c => " + c);
		System.out.println("a = c => " + a);
		System.out.println("a2 = new Cat() => " + a);
	}

}
