package br.com.aab.cracking.ocajp8.draft;

public interface Teste47Interface {
	void execute();
	default int executar() {
		return 10;
	}
	abstract void teste();
}
