package br.com.aab.cracking.ocajp8.practisetest5;

import java.util.Arrays;
import java.util.List;

public class ForExample {

	private static String[] wd = new String[] {"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"};
	
	public static void main(String[] args) {
		ForExample fe = new ForExample();
//		fe.execute();
		int[] valores = {200, 250, 300};
		System.out.println("Antes de chamar o metodo: " 
				+ valores[0] + " "
				+ valores[1] + " "
				+ valores[2] + " "
				);
		fe.execute2(valores);
		System.out.println("Depois de chamar o metodo: " 
				+ valores[0] + " "
				+ valores[1] + " "
				+ valores[2] + " "
				);
	}
	
	void execute2(int[] valores) {
		valores[0] = valores[0] * 2; 
		valores[1] = valores[1] * 2; 
		valores[2] = valores[2] * 2; 
	}
	
	void execute() {
		Contato contato = new Contato();
		contato.nome = "NOME";
		System.out.println("Antes da Chamada : " + contato.nome);
		new ForExample().exe5(contato);
		System.out.println("Depois da chamada : " + contato.nome);
	}
	
	static void exe5(Contato contato) {
		contato.nome = contato.nome + " Sobrenome";
	}
	
	class Contato {
		public String nome;
	}
	
	static void exe4() {
		for (int i = wd.length; i>=0; i--) System.out.println(wd[i]);
	}
	
	static void exe3() {
		int k = 0;
		for (int i = 10; i > 0; i--) {
			while(i > 3) i -= 3;
			k += 1;
		}
		System.out.println(k);
	}

	static void exe2() {
		List<Character> list1 = Arrays.asList('t', 'a', 'c', 'x', 'p');
		list1.forEach(p -> System.out.println(p.toString()));
		
		
	}
	
	static void exe1() {
		List<String> bottles = Arrays.asList("glass", "plastic");
		for (int type = 0; type < bottles.size();) {
			System.out.println(bottles.get(type));
			break;
		}
		System.out.println("end");
		
		System.out.println(wd.length);
	}
}
