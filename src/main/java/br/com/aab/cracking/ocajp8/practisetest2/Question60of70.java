package br.com.aab.cracking.ocajp8.practisetest2;

public class Question60of70 {

	public static void main(String[] args) {
		Integer i = 0;
		System.out.println("========== Integers ==========");
		System.out.println("i.BYTES = "+ i.BYTES);
		System.out.println("i.SIZE = "+ i.SIZE);
		
		Long l = 90234902l;
		System.out.println("l.BYTES = " + l.BYTES);
		System.out.println("l.SIZE = " + l.SIZE);
		System.out.println("===============================");
		
		System.out.println("========== Floats ==========");
		Float f = 1.421321f;
		System.out.println("f.BYTES = " + f.BYTES);
		System.out.println("f.SIZE = " + f.SIZE);
		
		Double d = 0.0;
		System.out.println("d.BYTES = "+ d.BYTES);
		System.out.println("d.SIZE = "+ d.SIZE);
		System.out.println("===============================");
	}

}
