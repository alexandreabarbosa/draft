package br.com.aab.cracking.ocajp8.practisetest2;

public class Quetion27of70 {

	public static void main(String[] args) {
		int x = 1;
		int y = 10;
		if ((x *= 3) == y) System.out.println(y);
		else System.out.println(x);
		
		int marks = 60;
		if (marks >= 40) System.out.println("C");
		else if (marks >= 60) System.out.println("B");
		else if (marks >= 75) System.out.println("A");
		else System.out.println("D");
		
		System.out.println(marks == 60 ? "TRUE" : "FALSE");
	}

}
