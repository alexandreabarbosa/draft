package br.com.aab.cracking.ocajp8.practisetest8;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

//class Problem implements RuntimeException {} 

public class BiggerProblem {

	interface Target {
		boolean needToAim(double angle);
	}
	
	static void prepare(double angle, Target t) {
		boolean ready = t.needToAim(angle);
		System.out.println(ready);
	}
	
	public static void main(String[] args) {
		prepare(45, d -> d > 5 || d < -5);
	}
	
	void exec2() {
		StringBuilder sb = new StringBuilder("12");
		sb = sb.append("3");
		System.out.println(sb);
		sb.reverse();
		System.out.println(sb);
		
		Predicate<String> pred1 = s -> false;
		Predicate<String> pred2 = (s) -> false;
//err		Predicate<String> pred3 = String s -> false;
		Predicate<String> pred4 = (String s) -> false;
	}
	
	void exec1() {
		StringBuilder sb = new StringBuilder("cl").insert(2,"own");
		System.out.println(sb);
		
		List<String> tools = new ArrayList<String>(1);
		tools.add("hammer");
		tools.add("nail");
		tools.add("hex key");

		System.out.println(tools.get(2));
		
	}

}
