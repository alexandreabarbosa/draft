package br.com.aab.cracking.ocajp8.practisetest1.question35;

public class Whiz {

	public static void main(String[] a) {
		int arr[][] = {{1, 3, 5}, {7, 8}};
//		out: for (int[] a : arr) {
//			for (int i : a) {
//				if (i == 7) continue;
//				System.out.println(i + " ");
//				if (i == 3) break;
//			}
//		}
	}

}
