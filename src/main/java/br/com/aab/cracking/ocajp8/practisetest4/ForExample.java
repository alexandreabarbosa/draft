package br.com.aab.cracking.ocajp8.practisetest4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ForExample {

	public static void main(String[] args) {
		int []nums[] = new int[2][1];
		
		ForExample fe = new ForExample();
		//fe.example9(args);
		fe.example7();
	}

	public void example9(String[] args) {
		String one = args[0];
		Arrays.sort(args);
		int result = Arrays.binarySearch(args, one);
		System.out.println(result); 
	}
	
//	public static void main(String... args) {
//		System.out.println(args[0]);
//	}
	
	public void example8() {
		String[] osnames = new String[] {"Mac", "Linux", "Windows"};
		Arrays.sort(osnames);
		System.out.println(Arrays.binarySearch(osnames, "Mac"));
	}
	
	public void example7() {
		String[] weeksDay = new String[] 
				{ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
		ArrayList<String> aWeekDay = new ArrayList<String>();
		Arrays.sort(weeksDay);
		Collections.addAll(aWeekDay, weeksDay);
		aWeekDay.forEach( p -> System.out.println( "binarySearch( " 
				+ p.toString() + " = "+ Arrays.binarySearch(weeksDay, p.toString()) + " )") );
		
	}
	
	public void example6() {
		boolean boools[][][], moreBools;
		char[][][] ticTacToe = new char[][][] {
			{
				{'A', 'B'}, 
				{'F', 'G', 'H', 'I'}, 
				{'K'}
			}, {
				{'S', 'R', 'T', 'U'}, 
				{'Q', 'P', 'O'}, 
				{'M', 'N'}
			}
		};
		System.out.println("First for => " + ticTacToe.length);
		for (int i = 0; i < ticTacToe.length; i++) {
			for (int j = 0; j < ticTacToe[i].length; j++) {
				for ( int k = 0; k < ticTacToe[i][j].length; k++) {
					System.out.println(ticTacToe[i][j][k] 
							+ " <= " + ticTacToe[i][j].length 
							+ " ( " + i + " / " + j + " / " + k + " )");
				}
			}
		}
	}
	
	public void example5() {
		String[] os = {"Mac", "Windows", "Linux"};
		Arrays.sort(os);
		System.out.println(Arrays.binarySearch(os, "Mac"));
	}
	
	public void example4() {
		String lion[] = new String[] {"lion"};
		String lion2[] = new String[] {};
		lion2 = new String[3];
		lion2[0] = "A";
		lion2[1] = "B";
		lion2[2] = "C";
		System.out.println(Arrays.toString(lion2));
		
	}

	public void example3() {
		//E #3
		//String[] numbers = {"1", "9", "10"};
		int[] numbers = {1, 9, 2, 7, 3, 10};
		Arrays.sort(numbers);
		System.out.println(Arrays.toString(numbers));
		
	}
	
	public void example2() {
		//E #2
		String[] weeksDay = new String[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friiday",
				"Satudrday" };
		for (int i=0; i < weeksDay.length; i++) {
			System.out.println(weeksDay[i]);
		}
//		ArrayList<String> aListWeeksDay = new ArrayList<String>();
//		Collections.addAll(aListWeeksDay, weeksDay);
//		aListWeeksDay.forEach(p -> System.out.println(p));
	}

	public void example1() {
		// E #1
		ForExample fe = new ForExample();
		fe.printName("Alexandre");
		fe.printName(new String[] { "Antonio" });
		fe.printNames(new String[] { "Gabriela", "Nunes", "Barbosa" });
	}

	public void printName(String... names) {
		System.out.println("Main varargs = " + Arrays.toString(names));
	}

	public void printNames(String[] names) {
		System.out.println("Main [] = " + Arrays.toString(names));
		System.out.println(names.length);
	}
}
