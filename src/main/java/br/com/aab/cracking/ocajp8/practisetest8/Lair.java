package br.com.aab.cracking.ocajp8.practisetest8;

class DragonException extends Exception {}

public class Lair {
	public void openDrawbridge() throws Exception {
		try {
			throw new Exception("This Exception");
		} catch (RuntimeException re) {
			throw new DragonException();
		} finally {
			throw new RuntimeException("Or maybe this one");
		}
	}
	public static void main(String[] args) throws Exception {
		new Lair().openDrawbridge();
		System.out.println("main");
	}

}
