package br.com.aab.cracking.ocajp8.practisetest1;

import java.util.Arrays;

public class ProgramArrays implements A{

	Double d1 = 2.0;
	Float f1 = 1.5F;
	Long l1 = new Long(2018);
	
	{ System.out.println("bloco de código"); }
	
	static { System.out.println("bloco de código estático"); }
	
	public ProgramArrays() {
		System.out.println("Construtor");
	}
	
	public static void main(String[] args) {
		Double d1 = 10.00/0.00;
		System.out.print("d1=");
		System.out.println(d1.isInfinite());
		System.out.println(A.s);
		int[] array1 = {2, -1, 4, 5, 3};
		int[] array2 = {2, -1, 4, 5, 3};
		System.out.println(array1 == array2);
		System.out.println(Arrays.equals(array1, array2));
		//System.out.println(Arrays.deepEquals(array1, array2));
		
		int y = 5;
		System.out.println();
		ProgramArrays pa = new ProgramArrays();
		pa.execute();
		
	}
	void execute() {
		System.out.println(A.s);
		System.out.println(A.i);
	}

}

interface A {
	static int i = 10;
	String s = "OCA";
}
