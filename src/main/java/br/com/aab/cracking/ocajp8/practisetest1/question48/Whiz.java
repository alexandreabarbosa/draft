package br.com.aab.cracking.ocajp8.practisetest1.question48;

public class Whiz {

	static int x = 10;
	public static void main(String[] args) {
		Whiz wh = new Whiz();
		wh.x = 5;
		int y = x / wh.x;
		System.out.print("y = ");
		System.out.println(y);
	}

}
