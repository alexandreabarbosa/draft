package br.com.aab.cracking.ocajp8.practisetest7;

class Math { public final double secret = 2; }

class ComplexMath extends Math { public final double secret = 4; }
 
public class InfiniteMath extends ComplexMath {

	public final double secret = 8;
	
	public static void main(String[] args) {
		Math im = new InfiniteMath();
		System.out.println(im.secret);
	}

	public String execute() {
		return "execute";
	}
}
