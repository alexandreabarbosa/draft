package br.com.aab.cracking.ocajp8.practisetest4;

import java.util.Arrays;
import java.util.Collections;

public class Question2 {

	public static void main(String[] args) {
		Integer[] a = {784,73,0,8,5,9,387};
		System.out.println("Antes do sort = " + Arrays.toString(a));
		
		Arrays.parallelSort(a);
		System.out.println("Depois do sort = " + Arrays.toString(a));
		
		Arrays.sort(a, Collections.reverseOrder());
		System.out.println("Sort Descendend = " + Arrays.toString(a));
		
		Arrays.fill(a, 2019);
		System.out.println("Array filled = " + Arrays.toString(a));
		
	}

}
