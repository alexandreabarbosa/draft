package br.com.aab.cracking.ocajp8.diagnostictest;

class Person {
	Person() {
		System.out.print("CP");
	}
	static { System.out.print("SP");}
}

class Manager extends Person {
	Manager() {
		System.out.print("CT");
	}
	{ System.out.print("IT"); }
	static { System.out.print("Manager Static block");}
}

public class Question44of70 {

	public static void main(String[] args) {
		Person p1 = new Manager();
	}

	
}
