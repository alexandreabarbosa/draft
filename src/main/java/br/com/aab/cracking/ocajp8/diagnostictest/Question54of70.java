package br.com.aab.cracking.ocajp8.diagnostictest;

class Print {
	private void p2(int i) {
		System.out.println(i*2);
	}
	static void print(int i) {
		System.out.println(i);
	}
	
}

public class Question54of70 {

	public static void main(String[] args) {
		Print p = new Print();
		Print.print(6);
		new Question54of70().exec();
	}

	private void exec() {
		
	}
}
