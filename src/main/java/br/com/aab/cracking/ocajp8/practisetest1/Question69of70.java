package br.com.aab.cracking.ocajp8.practisetest1;

import java.time.LocalDate;

public class Question69of70 {

	public static void main(String[] args) {
		System.out.println("abcdef".substring(4));
		System.out.println(LocalDate.of(2018, 5, 1).lengthOfMonth());
	}

}
