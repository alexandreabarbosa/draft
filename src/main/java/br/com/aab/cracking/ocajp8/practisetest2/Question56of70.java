package br.com.aab.cracking.ocajp8.practisetest2;

public class Question56of70 {

	public static void main(String[] args) {
		Double d = 10.0;
		int i = 10;
		Integer wi = 10;
		
		System.out.println("d equals i => " + d.equals(i));
		System.out.println("d equals wi => " + d.equals(wi));
		System.out.println("wi equals i => " + wi.equals(i));

		System.out.println("d == i => " + (d == i));
		System.out.println("wi == i => " + (wi == i));
	
	}

}
