package br.com.aab.cracking.ocajp8.practisetest1;


public class Question44of70 {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		System.out.println("Capacidade antes = " + sb.capacity());
		sb.append("456");
		System.out.println("Capacidade depois = " + sb.capacity());
	}

}
