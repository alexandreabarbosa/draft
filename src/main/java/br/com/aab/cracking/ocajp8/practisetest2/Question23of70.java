package br.com.aab.cracking.ocajp8.practisetest2;

public class Question23of70 {

	public static void main(String[] args) {
		int[] a = {1,2,3};
		for (int j : a) {
			if (j == 2) continue;
			for (int x = 0; x < 3; System.out.print(x)) {
				x++;
			}
		}
	}

}
