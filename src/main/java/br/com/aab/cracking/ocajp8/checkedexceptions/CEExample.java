package br.com.aab.cracking.ocajp8.checkedexceptions;

import java.io.IOException;

public class CEExample implements Runnable {

	public static void main(String[] args) throws IOException {
		Thread thrd = new Thread(new CEExample());
		thrd.start();
		try {
			thrd.sleep(1000);
		} catch (InterruptedException ie) {
			//ie.printStackTrace();
		}
		throw new IOException("Oops");
	}
	public void run() {
		while(true) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ie) {
				//ie.printStackTrace();
			}
			System.out.println("Alive!!!");
		}
	}
}
