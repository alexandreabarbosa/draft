package br.com.aab.cracking.ocajp8.practisetest8;

class CapsizedException extends Exception { }

class Transport {
	public int travel() throws CapsizedException { return 2; }
}

public class Boat {
	public int travel() throws Exception { return 4; }
	
	public static void main(String[] args) {
		try {
			System.out.println(new Boat().travel());
		} catch (Exception e) {
			System.out.println(8);
		}
	}

}
