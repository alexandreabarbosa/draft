package br.com.aab.cracking.ocajp8.practisetest4;

public class Question14 {

	public static void main(String[] args) {

		new Question14().execException();
		try {
//			run();
			
		} catch (RuntimeException e) {
			System.out.println("RuntimeException runtime");
		} catch (Exception e) {
			System.out.println("Exception here");
		} finally {
			System.out.println("Finally here");
		}
		System.out.println("Terminated");
	}

	private void execException() {
		throw new ArrayIndexOutOfBoundsException();
	}
	
	public static void run() {
		throw new RuntimeException();
	}
}
