package br.com.aab.cracking.ocajp8.oca.chap11.lambda.comparatorfromclass;

public class Water {
	private String source;
	
	public Water(String source) {
		this.source = source;
	}
	
	public String getSource() {
		return this.source;
	}
	
	public void setSource(String source) {
		this.source = source;
	}
	
	public String toString() {
		return this.source;
	}
}
