package br.com.aab.cracking.ocajp8.practisetest7;

abstract class Parallelogram {
	private int getEqualSides() { return 0;}
}

abstract class Rectangle extends Parallelogram {
	public static int getEqualSides() { return 2; }
}

public class Square extends Rectangle {
	public static int getEqualSides() { return 4; }
	public static void main(String[] args) {
		final Square myFigure = new Square();
		System.out.println(myFigure.getEqualSides());
	}
}
