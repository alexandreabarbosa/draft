package br.com.aab.cracking.ocajp8.practisetest3;

public class Question25 {

	public static void main(String[] args) {
		new Question25().iterator(new int[] {10,12,13});
	}
	
	void iterator(int[] ints) {
		for (int x = 0; x < ints.length; System.out.println(ints[x])) x++;
	}
}
