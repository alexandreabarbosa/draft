package br.com.aab.cracking.ocajp8.practisetest7;

interface MusicCreator { public Number play(); }
abstract class StringInstrument { public Long play() {return 3L;} }

public class Violin extends StringInstrument implements MusicCreator {
	public Long play() {
		return null;
	}
}
