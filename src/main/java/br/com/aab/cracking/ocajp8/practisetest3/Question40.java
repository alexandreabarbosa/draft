package br.com.aab.cracking.ocajp8.practisetest3;

public class Question40 {

	public static void main(String[] args) {
		int x = 0, y = 10;
		if (x++ > 1 && ++y > 10) {
			System.out.println("Primeiro if = true");
			System.out.println(x + y);
		} else {
			System.out.println("Primeiro if = false");
		}
		
		if (++y > 10 || ++x > 10) {
			System.out.println("Segundo if = true");
			System.out.println(x + y);
		} else {
			System.out.println("Segundo if = false");
		}
	}

}
