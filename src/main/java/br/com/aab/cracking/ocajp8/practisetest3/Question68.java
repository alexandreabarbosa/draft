package br.com.aab.cracking.ocajp8.practisetest3;

import java.util.ArrayList;
import java.util.List;

public class Question68 {

	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("C");
		list.add("E");
		list.add("D");
		list.add(1, "B");
		list.set(4, "F");
		System.out.println(list);
		System.out.println(list.size());
	}

}
