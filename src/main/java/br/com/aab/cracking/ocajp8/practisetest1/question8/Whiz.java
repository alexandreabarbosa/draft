package br.com.aab.cracking.ocajp8.practisetest1.question8;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Whiz {

	public static void main(String[] args) {
		try {
			throw method();
		} catch(IOException ioe) {
			System.out.println("caught");
		}
	}

	public static IOException method() {
		try {
			FileReader fr = new FileReader(new File("~/"));
			return new IOException();
		} catch (FileNotFoundException fnfe) {
			return new FileNotFoundException();
		}
	}
	
}
