package br.com.aab.cracking.ocajp8.practisetest4;

public class Question15 {

	public static void main(String[] args) {
		try {
			int a[] = new int[4];
			a[3] = (a[0] + a[1] / a[2]);
			System.out.println(a[3]);
//			System.out.println("a[0] = " + a[0]);
//			String nome = "Alexandre";
//			nome = null;
//			System.out.println(nome.length() / 2);
			int x = 0/1;
			
			String valor = "Valor";
			int y = Integer.parseInt(valor);
		} catch (NullPointerException e) {
			System.out.println("Null");
		} catch (ArithmeticException e) {
			System.out.println("A");
		} catch (NumberFormatException e) {
			System.out.println("F");
		} catch (Exception e) {
			System.out.println("E");
		}
	}

}
