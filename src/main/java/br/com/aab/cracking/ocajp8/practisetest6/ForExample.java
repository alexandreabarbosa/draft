package br.com.aab.cracking.ocajp8.practisetest6;

public class ForExample {

	private Object contents;
	static ForExample fe = new ForExample();
	
	public static void main(String[] args) {
		fe.execute();
	}
	
	short execute1() {
		return new Byte((byte) 6);
	}
	
	void execute() {
		fe.setContents(fe);
		fe.showContents();
		
		Contato contato = new Contato();
		contato.nome = "Alexandre";
		new ForExample().execute(contato);
		System.out.println(contato.nome);
	}
	
	void showContents() {
		System.out.println(this.contents);
	}
	
	static void execute(Contato contato) {
		contato.nome += " Antonio Barbosa";
		contato = null;
	}

	static class Contato {
		String nome;
	}

	public Object getContents() {
		return contents;
	}

	public void setContents(Object contents) {
		this.contents = contents;
	}
}
