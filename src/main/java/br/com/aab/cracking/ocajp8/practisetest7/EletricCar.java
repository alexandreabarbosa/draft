package br.com.aab.cracking.ocajp8.practisetest7;

class Automobile {
	private final String drive() { return "Driving vehicle"; }
}

class Car extends Automobile {
	protected String drive() { return "Driving car"; }
}

public class EletricCar extends Car {

	public final String drive() { return "Driving eletrict car"; }

	public static void main(String[] args) {
		final Car car = new EletricCar();
		System.out.println(car.drive());
	}

}
