package br.com.aab.cracking.ocajp8.practisetest1;

public class StaticClass {

	static { System.out.println("static block"); }
	
	public static void main(String[] args) {
		System.out.println("static void main");
		System.out.println(new StaticClass().execute());
		
	}
	
	public StaticClass() {
		System.out.println("construtor");
	}
	
	public static String execute() {
		System.out.println("static String execute()");
		return null;
	}
}
