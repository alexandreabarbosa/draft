package br.com.aab.cracking.ocajp8.practisetest5;

public class Question58 {

	static int i = 8;

	public static void main(String[] args) {
		int x = new Question58().change(i);
		System.out.println(x + i);
	}

	int change(int i) {
		i = 2;
		return i;
	}
}
