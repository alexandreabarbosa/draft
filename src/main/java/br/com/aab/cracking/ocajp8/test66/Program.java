package br.com.aab.cracking.ocajp8.test66;

public class Program {

	public static void main(String[] args) {
		String[] x = {"A", "B", "C", "D", "E"};
		nxt2(x);
		for (String string : x) {
			System.out.print(string + " ");
		}
		char[] chars = {'1', 'Z', '0', '-', '8', '1'};
		StringBuffer sb = new StringBuffer();
		sb.append(chars, 0, chars.length-1);
		sb.append("08");
		sb.setLength(4);
		System.out.println("\nantes => "+ sb);
		sb.insert(5, "10");
		System.out.println(sb);
	}
	static void nxt(Object o) {
		String[] y= (String[]) o;
		for(int i = 5, j = 0; i > 0; i--, j++) {
			y[j] = Integer.toString(i);
		}
	}
	static void nxt2(String[] o) {
		String[] y= o;
		for(int i = 5, j = 0; i > 0; i--, j++) {
			y[j] = Integer.toString(i * 10);
		}
	}
}
