package br.com.aab.cracking.ocajp8.practisetest3;

public class Question62 {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder("OCAJP");
		String strReverse = new String("StringBuilder reverse() => " + sb.reverse());
		String strCapacity = new String("StringBuilder capacity() =>  "+ sb.capacity());

		System.out.println(strReverse);
		System.out.println(strCapacity);
	}

}
