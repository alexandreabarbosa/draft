package br.com.aab.cracking.ocajp8.practisetest1;

public class Question42of70 implements Move{

	public static void main(String[] args) {
		//Move.print();
		System.out.println(Move.nome);
		System.out.println(Move.sobrenome);
	}

}

interface Move {
	
	String nome = "Alexandre";
	static String sobrenome = "Antonio Barbosa"; 
	
	//static void execute1();  ERROR this code require body code instead of semicolumn
	
	public static void main(String[] args) {
		System.out.println("Move");
	}
	public static void print() {
		System.out.println("vtnc");
	}
}