package br.com.aab.cracking.ocajp8.practisetest4;

import java.util.ArrayList;
import java.util.List;

public class Question61 {

	public static void main(String[] args) {
		
		int x = 10;
		
		System.out.println(x == 0?"0":x > 0?">":"<");
		
		List<String> list = new ArrayList<String>();
		list.add("A");
		list.add("C");
		list.add("E");
		list.add("D");
		list.add(1,"B");
		list.set(4, "F");
		System.out.println(list);
		
		System.out.println(list.set(3, "J"));
		
	}

}
