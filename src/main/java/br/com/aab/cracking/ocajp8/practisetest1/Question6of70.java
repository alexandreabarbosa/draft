package br.com.aab.cracking.ocajp8.practisetest1;

import java.util.Arrays;

public class Question6of70 {

	public static void main(String[] args) {
		Integer[] a1Integer = {2, -1, 4, 5, 3};
		Integer[] a2Integer = {2, -1, 4, 5, 3};
		int[] a1 = {2, -1, 4, 5, 3};
		int[] a2 = {2, -1, 4, 5, 3};
		System.out.println(a1 == a2);
		System.out.println(a1.equals(a2));
		System.out.println(Arrays.equals(a1, a2));
		System.out.println("\n================ Arrays.deepEquals ================");
		System.out.println(Arrays.deepEquals(a1Integer, a2Integer) + "");
	}

}
