package br.com.aab.cracking.ocajp8.practisetest2;

abstract class Animal {
	void run() {
		System.out.println("Animal run");
	}
	abstract void sound();
}

class Dog extends Animal {
	void sound() {
		System.out.println("Bark");
	}
	public void run() {
		System.out.println("Dogs runs");
	}
	void cry() {
		System.out.println("Cry");
	}
}

public class Question41of70 {

	public static void main(String[] args) {
		Animal dog = new Dog();
		dog.sound();
		dog.run();
	}

}
