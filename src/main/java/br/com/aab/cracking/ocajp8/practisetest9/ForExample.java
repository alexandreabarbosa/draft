package br.com.aab.cracking.ocajp8.practisetest9;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ForExample {

	private StringBuilder sb = new StringBuilder("radical ");
	
	public static void main(String[] args) {
		//new ForExample().exec6();
		Integer[] intVal = new Integer[10];
		for (Integer integer : intVal) {
			System.out.print(integer);
		}
	}
	
	void exec7() {
		LocalDate xmas = LocalDate.of(2016, 12, 25);
	}
	
	void exec6() {
		List<String> magazines = new ArrayList<String>();
		magazines.add("Readers digest");
		magazines.add("People");
		magazines.clear();
		magazines.add("The economist");
		magazines.remove(1);
		System.out.println(magazines.size());
	}
	
	void exec5() {
		StringBuilder sb = new StringBuilder();
		sb.append("red");
		sb.deleteCharAt(0);
		sb.delete(1, 3);
		System.out.println(sb);
	}
	
	void exec4() {
		LocalDate ld = LocalDate.of(2016, 12, 25);
		ld = ld.plusDays(100);
		System.out.println(ld);
	}
	
	void exec3() {
		String[] weekDays = {"Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"};
		Arrays.asList(weekDays).forEach(c -> System.out.println(c));
	}
	
	void exec2() {
		String s = "xptoabcdefg";
		System.out.println(s.contains("abc"));
	}

	void exec1() {
		sb = new StringBuilder("radical ").append("robots");
		System.out.println("1) " + sb.toString());
		
		sb = new StringBuilder("radical ")
				.delete(1, 100)
				.append("obots")
				.insert(1, "radical r");
		System.out.println("2) " + sb);
		
		sb = new StringBuilder("radical ")
				.insert(7, "robots");
		System.out.println("3) " + sb);
		
		sb = new StringBuilder("radical ")
				.insert(sb.length(), "robots");
		System.out.println("4)  "+ sb);
		
	}
}
