package br.com.aab.cracking.ocajp8.practisetest8;

public class ForExample33 {
	public static void main(String[] args) {
		String street = "350 5th Ave";
		String city = "New York";
		System.out.println(new ForExample33().getAdress(street, city));
	}
	
	public String getAdress(String street, String city) {
		try {
			return street.toString() + " - " + city.toString();
		} finally {
			System.out.println("Posted:");

		}
	}
}
