package br.com.aab.cracking.ocajp8.practisetest5;

import java.io.IOException;

public class Question62 {

	public void test(int i) {
		System.out.println(i);
	}

	public void test(float i) {
		System.out.println(i);
	}
	
	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder("Whiz");
		sb.append(new char[] {'l','a','b'},0,2);
		//System.out.println(sb);
//		new Question62().test(1.0f);    // ERRO porque falta o f
		
	}

}
