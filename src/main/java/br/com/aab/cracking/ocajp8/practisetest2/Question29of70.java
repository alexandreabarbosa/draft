package br.com.aab.cracking.ocajp8.practisetest2;

public class Question29of70 {

	public static void main(String[] args) {
		final int array[] = {1,2,3};
		switch(2) {
			/*
			 * ERRO : mesmo que a declaração do array seja final mas o valor de cada elemento não eh
			 * 		  logo isso causa um erro
			 */
//			case array[0]: System.out.println("A");   // ERRO
//			case array[1]: System.out.println("A");   // ERRO
			default: System.out.println("default"); break;
//			case array[2]: System.out.println("A");   // ERRO
		}
	}

}
