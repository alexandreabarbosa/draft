package br.com.aab.cracking.ocajp8.practisetest4;

public class Question18 {


	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder();
		sb.append("Whiz");
		sb = sb.append("lab");
		sb.append('s');
		sb.setLength(7);
		System.out.println(sb);
		
		new Question18().executeSB();
	}

	void executeSB() {
		StringBuilder sb2 = new StringBuilder("Alexandre");
		if (sb2.lastIndexOf("e") > 0) {
			System.out.println("sb2.lastIndexOf(e) = " + sb2.lastIndexOf("e"));
		} else if (sb2.indexOf("e") > 0) {
			System.out.println("sb2.indexOf(e) = " + sb2.indexOf("e"));
		} else {
			System.out.println(sb2);
		}
	}
}
