package br.com.aab.cracking.ocajp8.practisetest8;

class PrintException extends Exception {}
class PaperPrintException extends PrintException {}

public interface Printer {
	abstract int printData() throws PrintException;
}
