package br.com.aab.cracking.ocajp8.practisetest2;

public class Question11of70 {

	public static void main(String[] args) {
		try {
			new Question11of70().method();
		} catch (ArithmeticException ae) {
			System.out.println("ERRO Arithmetic : " + ae);
		} catch(Exception e) {
			System.out.println("Exception : " + e);
		} finally {
			
		}
	}
	
	public void method() throws ArithmeticException {
		for (int x = 0; x < 5; x++) {
			int y = (int) 5 / x;
			System.out.println(x);
		}
	}

}
