package br.com.aab.cracking.ocajp8.practisetest1;

public class Question54of70 {

	public static void main(String[] args) {
		double  intVal = 10.0/0.0;
		double doubleValorPri = 10.0/0.0;
		Double doubleNumber = 10.0/0.0;
		System.out.println(doubleNumber.isInfinite());
		System.out.println("doubleValorPri = ");
		System.out.println(doubleValorPri);
		String strDouble = "3";
		Double d = Double.parseDouble(strDouble);
		System.out.println("Conversao do Double eh = " + d.doubleValue());
		Long l = Long.parseLong(strDouble);
		System.out.println("Conversao do Long eh = " + l.longValue());
		
		// ERRO Long l1 = 3;
		Double d1 = 3.9;
		// ERRO Float f1 = 1.3;
		
	}

}
