package br.com.aab.cracking.ocajp8.practisetest3;

public class Question61 {

	static { 
		x = 10;
		y = 5;
	}
	
	static int x;   //Erro pois x tem que ser static
	final static int y;
	
	public static void main(String[] args) {
		try {
			Question61 q = new Question61();
			int c = q.x / y;
			System.out.println(c);
		} catch (ArithmeticException e) {
			System.out.println("Arithmetic Exception");
		}
	}

}
