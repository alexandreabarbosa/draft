package br.com.aab.cracking.ocajp8.practisetest1;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Question8of70 {

	public static void main(String[] args) {
		try {
			throw method();
		} catch (IOException ioe) {
			System.out.println("caught");
		}
	}

	public static IOException method() {
		//try {
			return new IOException();
		//} catch (FileNotFoundException fnfe) {    ERRO
			//return new FileNotFoundException();
		//}
	}
}
