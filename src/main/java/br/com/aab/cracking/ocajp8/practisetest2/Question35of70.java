package br.com.aab.cracking.ocajp8.practisetest2;

public class Question35of70 {

	public static void main(String[] args) {
		Integer i = 10;
		Double d = 10.0;
		int ii = 10;
		double dd = 10.0;
		
		System.out.println(i.equals(d));
		System.out.println(ii == dd);
		
		System.out.println("================================"
				+ "\nInteger hashcode = " + i.hashCode() 
				+ "\nDouble hashcode = " + d.hashCode()
				);
	}

}
