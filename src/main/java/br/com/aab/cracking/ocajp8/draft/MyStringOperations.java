package br.com.aab.cracking.ocajp8.draft;

public class MyStringOperations {

	private String hanna = "Did Hannah see bees? Hannah did.";
	private StringBuilder sbuilder = new StringBuilder();

	public static void main(String[] args) {
		new MyStringOperations().tutorial2a();
		new MyStringOperations().tutorial2b();
		new MyStringOperations().myConcat();
	}

	void tutorial2a() {
		System.out.println(hanna.length());
	}
	
	void tutorial2b() {
		System.out.println(hanna.charAt(2));
	}
	
	void myConcat() {
		hanna.concat(" - QUE PORRA EH ESSA?");
		System.out.println(hanna);
	}
}

