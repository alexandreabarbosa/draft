package br.com.aab.cracking.ocajp8.oca.chap11.lambda.anonymousinnerclass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.com.aab.cracking.ocajp8.oca.chap11.lambda.comparatorfromclass.Water;

public class WaterApp {

	public static void main(String[] args) {
		Water hardWater = new Water("Hard");
		Water softWater = new Water("Soft");
		Water boiledWater = new Water("Boiled");
		Water rawWater = new Water("Raw");
		Water rainWater = new Water("Rain");
		Water snowWater = new Water("Snow");
		Water filteredWater = new Water("Filtered");
		Water reverseOsmosisWater = new Water("ReverseOsmosis");
		Water deionezedWater = new Water("Deionezed");
		Water distilledWater = new Water("Distilled");
		List<Water> waterList = new ArrayList<Water>();
		waterList.add(hardWater);
		waterList.add(softWater);
		waterList.add(boiledWater);
		waterList.add(rawWater);
		waterList.add(rainWater);
		waterList.add(snowWater);
		waterList.add(filteredWater);
		waterList.add(reverseOsmosisWater);
		waterList.add(deionezedWater);
		waterList.add(distilledWater);
		System.out.println("Not sorted : " + waterList);
		Comparator<Water> waterSort = new Comparator<Water>() {
			@Override
			public int compare (Water w1, Water w2) {
				return w1.getSource().compareTo(w2.getSource());
			}
		};
		Collections.sort(waterList, waterSort);
		System.out.println("Sorted     : " + waterList);
		
	}

}
