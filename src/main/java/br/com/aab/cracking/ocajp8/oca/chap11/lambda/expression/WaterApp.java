package br.com.aab.cracking.ocajp8.oca.chap11.lambda.expression;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.IntStream;

import br.com.aab.cracking.ocajp8.oca.chap11.lambda.comparatorfromclass.Water;

public class WaterApp {

	public static void main(String[] args) {
		new WaterApp().exec2();	
	}
	
	void exec2() {

		Integer[] intList = {24, 40, 6, 5, 8, 0, 1, 3, 5, 7, 9};
		ArrayList<Integer> aIntegerList= new ArrayList<Integer>();
		Collections.addAll(aIntegerList, intList);
		System.out.println(aIntegerList);
		aIntegerList.stream().filter(i -> isCousin(i))
			.forEach(i -> System.out.println("Numero Primo = " + i.intValue()
			));
	}
	
	
	boolean isCousin(Integer number) {
		for (int x = 2; x < number; x++) {
			if ((number % x) == 0) return false;
		}
		return true;
	}

	void exec1() {
		Water hardWater = new Water("Hard");
		Water softWater = new Water("Soft");
		Water boiledWater = new Water("Boiled");
		Water rawWater = new Water("Raw");
		Water rainWater = new Water("Rain");
		Water snowWater = new Water("Snow");
		Water filteredWater = new Water("Filtered");
		Water reverseOsmosisWater = new Water("ReverseOsmosis");
		Water deionezedWater = new Water("Deionezed");
		Water distilledWater = new Water("Distilled");
		List<Water> waterList = new ArrayList<Water>();
		waterList.add(hardWater);
		waterList.add(softWater);
		waterList.add(boiledWater);
		waterList.add(rawWater);
		waterList.add(rainWater);
		waterList.add(snowWater);
		waterList.add(filteredWater);
		waterList.add(reverseOsmosisWater);
		waterList.add(deionezedWater);
		waterList.add(distilledWater);
		System.out.println("Not sorted : " + waterList);
		// Second Refactory
		// Comparator<Water> waterSort = (Water w1, Water w2) ->
		// w1.getSource().compareTo(w2.getSource());

		// First Refactory
		// Comparator<Water> waterSort = new Comparator<Water>() {
		// @Override
		// public int compare (Water w1, Water w2) {
		// return w1.getSource().compareTo(w2.getSource());
		// }
		// };

		Collections.sort(waterList, (w1, w2) -> w1.getSource().compareTo(w2.getSource()));

		System.out.println("Sorted     : " + waterList);

	}

}
