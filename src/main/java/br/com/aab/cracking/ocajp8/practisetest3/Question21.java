package br.com.aab.cracking.ocajp8.practisetest3;

import java.text.DecimalFormat;
import java.util.Date;

public class Question21 {

	public static void main(String[] args) {
		
		System.out.println(args.length);
		//System.out.format("Na data de  %td %tA %tB %tC %tF", new Date(), new Date(), new Date(), new Date(), new Date());
		
		Double dblValor = 90127343.8787823;
		DecimalFormat decFormat = new DecimalFormat("###,###.##########");
		//System.out.format(decFormat.format(dblValor));
		
		Integer intValor = Integer.MAX_VALUE;
		System.out.format("O valor eh %020d", intValor);
	}
}
