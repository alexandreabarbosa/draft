package br.com.aab.cracking.ocajp8.practisetest5;

public class Question38 {

	public static void main(String[] args) {
		float f = 12.0f;
		double d = f++;
		long l = 12;
		d--;
		d -= 0.3;
		System.out.print((f == d) + " ");
		System.out.print(d == l);
	}

}
