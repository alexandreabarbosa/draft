package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question66of70 {

	public static void main(String[] args) {
		char[] chars = {'1','Z','0','-','8','1'};

		StringBuffer sb = new StringBuffer();
		sb.append(chars, 0, chars.length-1);
		sb.append('0');
		sb.append('8');
		System.out.println(sb);
		
		
		String[] x = {"A", "B", "C", "D", "E"};
		method(x);
		for(String s: x) {
//			System.out.println(s);
		}
	}
	static void method(Object o) {
		String[] y = (String[]) o;
		for (int i = 5, j = 0; i > 0; --i, j++) {
			y[j] = Integer.toString(i);
		}
	}
}
