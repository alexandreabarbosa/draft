package br.com.aab.cracking.ocajp8.practisetest1;

public class Question23of70 {

	public static void main(String[] args) {
		/*
		 * NAO pode ser variavel nos CASES mas pode ser no switch!!!
		 */
		String s = "A";
		final String c1 = "A";
		final String c2 = "B";
		final String c3 = "C";
		switch(s) {
			case c1: {System.out.println("A");};
			default: {System.out.println("default");};
			case c2: {System.out.println("B");};
			case c3: {System.out.println("C");};
		}		
	}

}
