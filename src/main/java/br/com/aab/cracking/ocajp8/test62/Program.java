package br.com.aab.cracking.ocajp8.test62;

public class Program {
	public static void main(String[] args) {
		Animal animal = new Dog();
		animal.sound();
	}
}

class Animal {
	int sound() {
		System.out.println("Animal sound");
		return 1;
	}
}

class Dog extends Animal {
	protected int sound() {
		System.out.println("Dog sound");
		return 1;
	}
}
