package br.com.aab.cracking.ocajp8.oca.chap11.lambda.fi;

import javafx.scene.paint.Color;

public class Planet {
	private String name = "Unknown";
	private Color primaryColor = Color.WHITE;
	private Integer numberOfMooms = 0;
	private Boolean ringed = false;
	public Planet(String name, Color primaryColor, Integer numberOfMooms, Boolean ringed) {
		this.name = name;
		this.primaryColor = primaryColor;
		this.numberOfMooms = numberOfMooms;
		this.ringed = ringed;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Color getPrimaryColor() {
		return primaryColor;
	}
	public void setPrimaryColor(Color primaryColor) {
		this.primaryColor = primaryColor;
	}
	public Integer getNumberOfMooms() {
		return numberOfMooms;
	}
	public void setNumberOfMooms(Integer numberOfMooms) {
		this.numberOfMooms = numberOfMooms;
	}
	public Boolean getRinged() {
		return ringed;
	}
	public void setRinged(Boolean ringed) {
		this.ringed = ringed;
	}
}
