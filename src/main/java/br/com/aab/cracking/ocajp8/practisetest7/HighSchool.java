package br.com.aab.cracking.ocajp8.practisetest7;

import java.io.FileNotFoundException;
import java.io.IOException;

class School {
	public int getNumberOfStudentsPerClassRoom(String... students) 
			throws IOException { return 3; }
	public int getNumberOfStudentsPerClassRoom() 
			throws IOException { return 9; }
}

public class HighSchool extends School {

	public int getNumberOfStudentsPerClassRoom() 
			throws FileNotFoundException { return 2; }
	
	public static void main(String[] args) throws IOException {
		School school = new HighSchool();
		System.out.println(school.getNumberOfStudentsPerClassRoom());
	}

}
