package br.com.aab.cracking.ocajp8.test70;

public class Whiz {

	public static void main(String[] args) {
		Comparable c1 = "ABC";
		Comparable c2 = new String("ABC");
		System.out.println(c1.equals(c2));
	}

}
