package br.com.aab.cracking.ocajp8.test6;

import java.util.Arrays;

public class Program {
	public static void main(String[] args) {
		//String[][] strings = { {"A", "Z"}, {"C", "D", "S"}, {"L"} };
		String[] strings = {"B", "D", "K", "0", "9", "X", "A", "M"};
		Arrays.sort(strings);
		for (String string : strings) {
			System.out.println(string);
		}
//		for (String[] str: strings) {
//			for(String s: str) {
//				System.out.println(s);
//			}
//		}
	}

}
