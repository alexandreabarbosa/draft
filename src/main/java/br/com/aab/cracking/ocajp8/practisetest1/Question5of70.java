package br.com.aab.cracking.ocajp8.practisetest1;

import java.util.Arrays;

public class Question5of70 {

	public static void main(String[] args) {
		String[] strings = {"s", "N", "L", "n", "O", "S"};
		Arrays.sort(strings);
		for (String s : strings) 
			System.out.print(s);
	}

}
