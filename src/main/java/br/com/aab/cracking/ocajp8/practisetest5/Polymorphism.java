package br.com.aab.cracking.ocajp8.practisetest5;

public class Polymorphism {

	public static void main(String[] args) {
		Double myValue = 3.00;
		System.out.println("Antes do metodo: " + myValue);
		new Polymorphism().execute(myValue);
		System.out.println("Depois do metodo: " + myValue);
	}

	static void execute(Double longValue) {
		longValue = longValue + 1.2;
	}
}
