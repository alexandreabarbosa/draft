package br.com.aab.cracking.ocajp8.diagnostictest;

class Sup {

	protected void method() {
		System.out.println("Sup");
	}
}

class Sub extends Sup {
	public final void method() {
		System.out.println("Sub");
	}
}

public class Question46of70 {

	public static void main(String[] args) {
		Sub sub = new Sub();
		sub.method();
	}

}
