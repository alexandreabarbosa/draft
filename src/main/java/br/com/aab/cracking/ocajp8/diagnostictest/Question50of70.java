package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question50of70 {

	final static int x;// ERRO SEM O static
	final static int y;
	
	static {
		x = 10;
		y = 5;
	}
	
	public static void main(String[] args) {
		try {
			Question50of70 pr = new Question50of70();
			int c = pr.x / y;
			System.out.println(c);
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
	}

}
