package br.com.aab.cracking.ocajp8.practisetest5;

public class Question5 {

	public static void main(String[] args) {
		long [][] l2d;
		long [] l1d = {1,2,3};
		Object o = l1d;
		for (long l : (long[])o) System.out.println(l);
		l2d = new long[3][3];
		//l2d[0][0] = (long[])o; ERRO
	}

}
