package br.com.aab.cracking.ocajp8.practisetest1;

interface I { void method(); }

class Aa implements I {

	void Aa (String s) { }
	
	public void method() {
		System.out.println("A");
	}
	
}

class C extends Aa implements I{
	public void method() {
		System.out.println("C");
	}
}

public class Question40of70 {

	public static void main(String[] args) {
		C c2 = new C();
		Aa ab = (Aa)c2;
		Aa aa = new Aa();
		C c1 = (C)aa;    //EEEERRRROOOO
		c1.method();
	}

}
