package br.com.aab.cracking.ocajp8.practisetest6;

public class Puppy {

	public static int wag = 5;
	
	public Puppy(int wag) {
		this.wag = wag;
	}
	
	public static void main(String[] args) {
		System.out.println(new Puppy(2).wag);
	}

}
