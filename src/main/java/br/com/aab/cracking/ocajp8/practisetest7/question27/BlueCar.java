package br.com.aab.cracking.ocajp8.practisetest7.question27;

abstract class Car {
	static { System.out.println("Bloco static abstract class Car - EM CIMA "); }
	
	public Car(String name) {
		super();
		System.out.println("Construtor abstract class Car ");
	}
	
	{ System.out.println("Bloco perdido dentro da abstract class Car "); }

	static { System.out.println("Bloco static abstract class Car - EM BAIXO "); }

}
public class BlueCar extends Car {

	{ System.out.println("Bloco perdido dentro da BlueCar "); }

	public BlueCar() {
		super("Blue");
		System.out.println("Construtor da BlueCar ");
	}
	static { System.out.println("Bloco static dentro da BlueCar - EM EM CIMA"); }

	public static void main(String[] args) {
		System.out.println("Bloco main BlueCar");
		new BlueCar();
	}
	
	static { System.out.println("Bloco static dentro da BlueCar - EM BAIXO"); }

}
