package br.com.aab.cracking.ocajp8.oca.chap11.lambda.expression;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ForExample {

	private ArrayList<String> fabricantesList = new ArrayList<String>();

	public static void main(String[] args) {
		ForExample fe = new ForExample();
		//fe.run();
		fe.executa();
		
	}
	
	public ForExample() {		
		fabricantesList.add("VM");
		fabricantesList.add("Ford");
		fabricantesList.add("Toyota");
		fabricantesList.add("Honda");
		fabricantesList.add("Fiat");
		fabricantesList.add("Hyundai");
		fabricantesList.add("Citroen");
	}

	public void executa() {
		fabricantesList.stream().filter(p -> p.contains("n"))
					.forEach(p -> System.out.println(p.toString()));
	}
		
	void run() {
		List<String> lString = new ArrayList<String>();
		fabricantesList.forEach(f -> System.out.println(f));
		fabricantesList.stream().filter(f -> f.equals("Citroen")).
			forEach(f -> System.out.println("Teremos C4 Lounge! - " + f.toString()));
		
//		for (String s : fabricantesList) {
//			if (s.equals("Citroen")) {
//				System.out.println("Teremos C4 Lounge!");
//			}
//		}
	}
}
