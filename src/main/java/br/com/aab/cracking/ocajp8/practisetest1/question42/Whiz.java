package br.com.aab.cracking.ocajp8.practisetest1.question42;

public class Whiz {

	public static void main(String[] args) {
		Move.print();
	}

	interface Move {
		public static void main(String[] args) {
			System.out.println("Move");
		}
		public static void print() {}
	}
}
