package br.com.aab.cracking.ocajp8.practisetest5;

public class Question10 {

	public static void main(String[] args) {
		new Question10().go();
	}

	void go() {
		System.out.print("A ");
		try {
			run(0);
			System.out.print("B ");
//		} catch (ArithmeticException e) {
//			System.out.print("Arithmetic ");
		} catch (Exception e) {
			System.out.print("C ");
		} finally {
			System.out.print("D ");
		}
	}
	
	void run(int i) {
		try {
			System.out.print("E ");
			int x = 5/i;
			System.out.print("F ");
//		} catch (ArithmeticException e) {
//			System.out.print("Arithmetic ");
		} catch (NumberFormatException e) {
			System.out.print("G ");
		} finally {
			System.out.print("H ");
		}
		
	}
}
