package br.com.aab.cracking.ocajp8.practisetest1;

public class Question45of70 {

	public static void main(String[] args) {
		StringBuilder sb = new StringBuilder("Java");
		StringBuilder sb0 = new StringBuilder("Java");
		String s = new String("Java");
		
		System.out.println("sb.equals(sb0) " + sb.equals(sb0));
		System.out.println("sb.equals(s) " + sb.equals(s));
		System.out.println("s.equals(sb) " + s.equals(sb));
		System.out.println("sb.toString().equals(sb0.toString()) " + sb.toString().equals(sb0.toString()));

		double number = 21.0D;
	}

}
