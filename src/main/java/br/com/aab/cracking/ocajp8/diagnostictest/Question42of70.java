package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question42of70 {

	public static void main(String[] args) {
		B a = new B();
		a.method();
	}

}

class A {
	static void method() {
		System.out.println("A B");
	}
}
class B extends A {
	protected static void method() {
		System.out.println("C D");
	}
}