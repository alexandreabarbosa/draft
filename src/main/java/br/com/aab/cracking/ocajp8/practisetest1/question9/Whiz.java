package br.com.aab.cracking.ocajp8.practisetest1.question9;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Whiz {

	public static void main(String[] args) {
		try {
			method();
		} catch (FileNotFoundException fnfe) {
			System.out.println("File Not Found");
		} catch (IOException ioe) {
			System.out.println("IO Exception");
		}
	}

	public static void method() throws IOException {
		throw new FileNotFoundException();
	}
}
