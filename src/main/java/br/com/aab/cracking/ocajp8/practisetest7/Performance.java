package br.com.aab.cracking.ocajp8.practisetest7;

interface SpeakDialouge { default int talk(String x) {return 7;} }
interface SpeakMonolouge { default int talk() {return 5;}}

public class Performance implements SpeakDialouge, SpeakMonolouge {

	public int talk(String... x) {
		return x.length;
	}
	public static void main(String[] notes) {
		System.out.println(new Performance().talk(notes));
	}

}
