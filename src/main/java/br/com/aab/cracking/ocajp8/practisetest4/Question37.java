package br.com.aab.cracking.ocajp8.practisetest4;

public class Question37 {

	public static void main(String[] args) {
		int y = 10;
		
		if(y++ == 10)
			if(y-- == 10)
				if(y == 10);
				else y*=3;
			else y*=2;
		System.out.println(y);
	}

}
