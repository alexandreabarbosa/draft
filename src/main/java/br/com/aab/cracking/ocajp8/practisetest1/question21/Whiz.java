package br.com.aab.cracking.ocajp8.practisetest1.question21;

public class Whiz {
	static int x = 50;
	public final static void main(String[] args) {
		Integer[] a = new Integer[2];
		a[1] = 10;
		for (Integer integer : a) {
			System.out.println(integer);
		}
	}
}
