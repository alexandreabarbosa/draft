package br.com.aab.cracking.ocajp8.practisetest1;

import java.io.FileNotFoundException;
import java.io.IOException;

public class Question9of70 {

	public static void main(String[] args) {
		try {
			method();
		} catch(FileNotFoundException fnfe) {
			System.out.println("FileNotFoundException");
		} catch(IOException ioe) {
			System.out.println("IOException");
		}
	}

	public static void method() throws IOException {
		throw new FileNotFoundException();
	}
}
