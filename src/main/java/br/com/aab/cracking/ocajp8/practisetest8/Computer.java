package br.com.aab.cracking.ocajp8.practisetest8;

public class Computer {

	public void compute() throws Exception {
		throw new RuntimeException("Error processing request!");
	}
	
	public static void main(String[] args) {
		try {
			//new Computer().compute();
			System.out.println("Ping");
		} catch(NullPointerException npe) {
			System.out.println("Pong");
			throw npe;
		}
	}

}
