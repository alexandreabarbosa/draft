package br.com.aab.cracking.ocajp8.draft;


class A {
	static void method() {
		System.out.println(" A B ");
	}
}

class B extends A {
	protected static void method() {
		System.out.println(" C D ");
	}
}

public class Test42Program {

	public static void main(String[] args) {
		A a = new B();
		a.method();
		A.method();
		B.method();
	}
}
