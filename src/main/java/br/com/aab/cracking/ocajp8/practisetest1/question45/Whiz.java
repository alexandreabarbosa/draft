package br.com.aab.cracking.ocajp8.practisetest1.question45;

public class Whiz {

	public static void main(String[] args) {
		StringBuffer sb2 = new StringBuffer();
		sb2.append("Java");
		StringBuffer sb = new StringBuffer();
		sb.append("Java");
		String s = new String("Java");
		System.out.println(sb.toString().equals(sb2.toString()));
		
		float f = 21.3F;
		char c = 65;
		System.out.println(c);
		byte b = 127;
	}

}
