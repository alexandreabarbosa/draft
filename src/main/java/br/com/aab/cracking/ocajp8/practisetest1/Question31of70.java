package br.com.aab.cracking.ocajp8.practisetest1;


public class Question31of70 {

	public static void main(String[] args) {
		int x = 0;
		
		String[] animal = new String[3];
		do { animal[x] = "Cat"; x++; } while(false);
		do { animal[x] = "Dog"; } while (x > animal[x].length());
	
		new Question31of70().execute();
	}

	void execute() {
		
	}
	
	public Question31of70() {
		super();
	}
	
	void executar() {
		
	}
}
