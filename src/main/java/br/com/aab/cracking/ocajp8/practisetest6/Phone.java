package br.com.aab.cracking.ocajp8.practisetest6;

public class Phone {

	private int size;
	public Phone(int size) {this.size = size;}
	
	public static void sendPhone(Phone p, int size) {
		p = new Phone(size);
		p.size = 4;
		
	}
	
	public static void main(String... params) {
		final Phone phone = new Phone(3);
		sendPhone(phone, 7);
		System.out.println(phone.size);
	}
}
