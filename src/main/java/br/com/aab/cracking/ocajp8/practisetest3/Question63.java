package br.com.aab.cracking.ocajp8.practisetest3;

public class Question63 {

	public static void main(String[] args) {
		
		
		String str = "Alexandre";
		str = str.replace("e", "i");
		System.out.println(str);
		System.out.println("[" + str.substring(0, 0)+ "]");
		System.out.println(str.length());
		
		String[] strArray = {"A", "B", "C"};
		System.out.println(strArray.length);
		
		StringBuilder sb = new StringBuilder("aAaA");
		sb.insert(sb.lastIndexOf("A"), true);
		System.out.println(sb);
		
		StringBuffer strBuffer = new StringBuffer("Alexandre");
		System.out.println(strBuffer);
	}

}
