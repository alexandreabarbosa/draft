package br.com.aab.cracking.ocajp8.practisetest1.question32;

public class Whiz {

	public static void main(String[] args) {
		
		int arr[][][] = {{ {1,3,5}, {7,8}, {8, 2, 5} }, {{6, 8, 0}, {1}, {8, 6, 1}} };
		for (int[][] a : arr) {
			for(int[] b : a) {
				for (int c : b) {
					System.out.println(c);
				}
			}
		}
	}

}
