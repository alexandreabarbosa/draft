package br.com.aab.cracking.ocajp8.practisetest6;

public class Question7 {

	static Integer i;
	
	public static void main(String[] args) {
		try {
			System.out.println(i.toString());
		} catch (NullPointerException e) {
			for (StackTraceElement stackElements : e.getStackTrace()) {
				System.out.println(stackElements.toString());
			}
		} catch (RuntimeException e) {
			System.out.println("RuntimeException");
			throw e;
		} catch (Exception e) {
			System.out.println("Exception");
			System.out.println(e.getMessage());
		} finally {
			System.out.println("Finally");
		}
	}

}
