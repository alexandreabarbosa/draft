package br.com.aab.cracking.ocajp8.practisetest4;

class Rectangle {
	
	int _ = $;
	static int $ = 2;
	
	public int height;
	public int width;
	public int area;
	
	public Rectangle() {}

	protected Rectangle(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	private Rectangle(int area) { this.area = area; }
	
}

public class Question53 extends Rectangle{
	
//	public Question53(int w, int h) {
//		super(w,h);
//	}
	
//	public Question53(int a) { super(a); }  ERRO
	
	public static void main(String[] args) {
		//Question53 q = new Question53();
		//Question53 q = new Question53(0,0);   ERRO
		//Question53 q = new Question53(0);     ERRO
		
	}

}


