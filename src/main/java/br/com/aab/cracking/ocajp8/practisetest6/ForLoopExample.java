package br.com.aab.cracking.ocajp8.practisetest6;

public class ForLoopExample {

	public static void main(String[] args) {
		new ForLoopExample().executeForLoop();
	}
	
	void executeForLoop() {
		for (int i = 0; i<10;System.out.println(i++));
	}
}
