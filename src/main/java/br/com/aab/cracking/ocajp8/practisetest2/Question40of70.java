package br.com.aab.cracking.ocajp8.practisetest2;

class Aaa {
	protected void run() { System.out.println("Aaa"); }
}

class Question40of70 extends Aaa{

	@Override
	public void run() { System.out.println("Bbb"); }
	
	public static void main(String[] args) {
		Question40of70 q = new Question40of70();
		q.run();
		
	}

}
