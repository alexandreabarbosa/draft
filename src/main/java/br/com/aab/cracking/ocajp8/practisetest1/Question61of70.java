package br.com.aab.cracking.ocajp8.practisetest1;

public class Question61of70 {

	private String code = "1Z0-808";
	{ System.out.println(code + " - Bloco de código"); }
	private static int QUESTIONS = 90;
	static { System.out.println(QUESTIONS + " - Bloco estatico 1"); }
	static { QUESTIONS -= 13; System.out.println(QUESTIONS + " - Bloco estatico 2"); }
	
	public Question61of70() {
		System.out.println("Construtor");
	}
	public static void main(String[] args) {
		new Question61of70();
	}

}
