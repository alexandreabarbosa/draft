package br.com.aab.cracking.ocajp8.practisetest1;

public class Question27of70 {

	public static void main(String[] args) {
		int y = 5;
		if (false && y++ == 11)
			System.out.println(y);
		else if(true || --y == 4)
			System.out.println(y);
		else {} // ERRO (y == 5) {}
		
		for(; ; System.out.println("contador = " ));
	}

}
