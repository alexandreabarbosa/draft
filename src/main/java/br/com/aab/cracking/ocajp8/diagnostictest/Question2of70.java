package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question2of70 {

	public static void main(String[] args) {
		new Question2of70().numberWithBinary();
	}
	
	private void numberWithBinary() {
		int intBinVal = 0b101;
		int ints[] = new int[intBinVal];
		int len = ints.length;
		for (int i : ints)
			System.out.println(i);
	}
	
	
}
