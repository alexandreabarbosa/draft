package br.com.aab.cracking.ocajp8.practisetest7;

class Computer {
	private final int process() { return 5; }
}
public class LapTop extends Computer {

	final int process() { return 3; }
	
	public static void main(String[] args) {

	}

}
