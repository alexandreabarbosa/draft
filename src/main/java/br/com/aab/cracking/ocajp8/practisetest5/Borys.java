package br.com.aab.cracking.ocajp8.practisetest5;

public class Borys {

	static private double custo = 100;
	
	public static void main(String[] args) {
		Borys borys = new Borys();
		borys.do1();
	}
	
	void do1() {
		Gael gael = new Gael();
		System.out.println(gael.execute(this.custo));
	}
	 
	public double execute(double custo) {
		return custo * 1.2;
	}
	
	public class Gael extends Borys {
		
		public double execute(double custo) {
			return custo * 1.3;
		}
		public double execute(int custo) {
			return custo * 1;
		}
	}
}
