package br.com.aab.cracking.ocajp8.freetest;

public class Draft {

	public static void main(String[] args) {
		//new Draft().negativeArray();
		//new Draft().doubleWithString();
		new Draft().integerWithString();
	}

	private void negativeArray() {
		int array[] = new int[-2];
		array[-2] = -2;
		array[-1] = -1;
		array[0] = 0;
		System.out.println(array[-2]);
		System.out.println(array[-1]);
		System.out.println(array[0]);
	}
	
	private void doubleWithString() {
		Double doubleNumber = new Double("128D");
		System.out.println(doubleNumber);
	}

	private void integerWithString() {
		
		Integer integerVal = new Integer("808.1");
		System.out.println(integerVal);
	}
}
