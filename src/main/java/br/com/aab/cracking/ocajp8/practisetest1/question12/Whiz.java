package br.com.aab.cracking.ocajp8.practisetest1.question12;

import static java.lang.Math.*;

public class Whiz {
	private static String var1 = "String";
	
	public static void main(String[] args) {
		try {
			System.out.println(PI);
			print();
		} catch(Exception e) {
			
		}
	}
	
	static void print() {
		try {
			//throw new NullPointerException();
			Object o = var1;
		} catch(ClassCastException cce) {
			System.out.println("Class Cast");
		} finally {
			System.out.println("Final");
		}
		System.out.println("OCAJP");
	}
}
