package br.com.aab.cracking.ocajp8.practisetest1;

class Animal {
	void eat() {System.out.println("Animal eats");}
}

class Bird extends Animal {
	void eat() { System.out.println("Birds eats"); }
	void print() { super.eat(); } 
}
public class Question37of70 {

	public static void main(String[] args) {
		Bird ab = new Bird();
		ab.print();
	}

}
