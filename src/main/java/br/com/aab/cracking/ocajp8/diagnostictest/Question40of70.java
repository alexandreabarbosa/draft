package br.com.aab.cracking.ocajp8.diagnostictest;

public class Question40of70 {

	public static void main(String[] args) {
		String s1 = "Rekha";
		//String s2 = "Rekha";
		String s2 = new String("Rekha");
		
		System.out.println(s1.equals(s2));
		System.out.println(s1 == s2);
		
		System.out.println(s1 + " - hashcode = " + s1.hashCode());
		System.out.println(s2 + " - hashcode = " + s2.hashCode());
	}

}
