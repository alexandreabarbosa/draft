package br.com.aab.cracking.ocajp8.practisetest7;

interface Horn { public Integer play(); }

abstract class WoodWind { public Short play2() { return 3; } }

public final class Saxophone extends WoodWind implements Horn {
	public Integer play() {
		return null;
	}
}
