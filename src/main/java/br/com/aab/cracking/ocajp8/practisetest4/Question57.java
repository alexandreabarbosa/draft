package br.com.aab.cracking.ocajp8.practisetest4;

public class Question57 {

	public static void main(String[] args) {

		Boolean b = Boolean.valueOf(true);
		System.out.println(b);
		Boolean b2 = Boolean.parseBoolean("true");
		System.out.println(b2);
		Boolean b3 = Boolean.getBoolean("TRUE");
		System.out.println(b3);
		
		System.out.println("BYTE");
		Byte bx = 12;
		System.out.println(bx.SIZE);
		System.out.println(bx.BYTES);
		
		System.out.println("SHORT");
		Short sx = 11;
		System.out.println(sx.SIZE);
		System.out.println(sx.BYTES);
		
		System.out.println("INTEGER");
		Integer ix = 8;
		System.out.println(ix.SIZE);
		System.out.println(ix.BYTES);
		
		
	}

}
