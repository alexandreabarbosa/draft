package br.com.aab.cracking.ocajp8.practisetest1.question23;

public class Whiz {

	public static void main(String[] args) {
		String s = "A";
		String c1 = "A";
		String c2 = "B";
		String c3 = "C";
		
		switch(s) {
			case "A": {
				System.out.println("A");
			}
			case "B": {
				System.out.println("B");
			}
			default: {
				System.out.println("default");
				break;
			}
			case "C": {
				System.out.println("C");
			}
		}
	}

}
