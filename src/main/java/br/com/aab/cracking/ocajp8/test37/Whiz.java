package br.com.aab.cracking.ocajp8.test37;

public class Whiz {
	public static void main(String[] args) {
		int x = 10;
		int y = 15;
		int z = 20;
		int a = 0;
		//Bitwise exclusive OR  que é o mesmo que  XOR
		System.out.println(x == 10 ^ y == 15 ^ x > 9 ^ x > 8 ^ x > 7);
	}

}
