package br.com.aab.cracking.ocajp8.practisetest1;

class Employee {
	Employee(String s, int i) {
		name = s;
		age = i;
	}
	String name;
	int age;
}

public class Question62of70 {

	public static void main(String[] args) {
		Employee e = new Employee("OCA", 90);
		System.out.println("e.age - Antes = " + e.age);
		updateAge(e, 95);
		System.out.println("e.age - Depois = " + e.age);
	}

	public static void updateAge(Employee e, int i) {
		e.age = i;
	}
}
